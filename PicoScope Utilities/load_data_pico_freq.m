function [c1_f, c1_dBV, c2_f, c2_dBV] = load_data_pico_freq(file_name)
    name_len = length(file_name);
    file_name_no_ext = file_name(1:name_len-4);
    
    T = readtable(file_name);
    TT = T{:,:};

    fid = fopen(file_name);    
    line = fgetl(fid);
    line = fgetl(fid);
    fclose(fid);
    units = split(line, ',');
    switch units{1}
        case '(kHz)'
            freq_factor = 1e3;
        otherwise
            disp(line);
            disp('Error, not recognized');
    end
    
    c1_f = freq_factor*TT(:, 1);
    c2_f = freq_factor*TT(:, 1);
    c1_dBV = TT(:, 2);
    c2_dBV = TT(:, 3);
end