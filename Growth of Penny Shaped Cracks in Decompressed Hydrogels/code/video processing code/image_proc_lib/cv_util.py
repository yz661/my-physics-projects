import cv2
from matplotlib import pyplot as plt
import numpy as np
from IPython.display import clear_output


def disp_cv_img(img):
    try:
        img2 = img[:, :, ::-1]
        plt.imshow(img2)
    except:
        plt.imshow(img, cmap='gray', vmin=0, vmax=255)
    plt.show()


def update_progress(progress):
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    block = int(round(bar_length * progress))

    clear_output(wait=True)
    text = "Progress: [{0}] {1:.1f}%".format("#" * block + "-" * (bar_length - block), progress * 100)
    print(text)


def manual_select_roi(frame, reduction_ratio=0.5):
    reduced_frame = cv2.resize(frame, dsize=None, fx=reduction_ratio, fy=reduction_ratio)
    r = cv2.selectROI(reduced_frame)
    cv2.destroyAllWindows()
    original_roi = np.array(r) / reduction_ratio
    return original_roi.astype(int)


def adjust_img_size(raw_img, disp_size=800, is_make_square=False):
    disp_ratio = disp_size / max(raw_img.shape)
    resized_img = cv2.resize(raw_img,
                             dsize=None,
                             fx=disp_ratio,
                             fy=disp_ratio,
                             interpolation=cv2.INTER_NEAREST)
    if not is_make_square:
        return resized_img, disp_ratio
    else:
        padded_img = make_square(resized_img)
        return padded_img, disp_ratio


def make_square(raw_img):
    disp_size = max(raw_img.shape)
    padded_img_shape = np.array(raw_img.shape)
    padded_img_shape[0] = disp_size
    padded_img_shape[1] = disp_size
    padded_img = np.full(padded_img_shape, 0, dtype=np.uint8)
    img_x_width = raw_img.shape[0]
    img_y_width = raw_img.shape[1]
    x_offset = (disp_size - img_x_width) // 2
    y_offset = (disp_size - img_y_width) // 2
    padded_img[x_offset:x_offset + img_x_width, y_offset:y_offset + img_y_width] = raw_img
    return padded_img


def get_roi(img, roi):
    if img is None or roi is None:
        print("get_roi: something is None!")
        return None
    return img[int(roi[1]):int(roi[1] + roi[3]),
               int(roi[0]):int(roi[0] + roi[2])]


class VideoManager(object):
    def __init__(self, vid_file_name, fps=25):
        self.cap = cv2.VideoCapture(vid_file_name)
        self.frame_cnt = 0
        self.curr_frame = None
        self.fps = fps

    def retrieve_frame(self, skip_n=0):
        # do the skipping
        for i in range(skip_n + 1):
            ret, self.curr_frame = self.cap.read()
            self.frame_cnt = self.frame_cnt + 1
        return self.curr_frame

    def read(self):
        return self.retrieve_frame()

    def load_to_frame(self, frame_cnt, monitor=True):
        if self.frame_cnt > frame_cnt:
            print("load to frame cannot load in reverse")
            return None
        while self.frame_cnt < frame_cnt:
            self.read()
            if monitor:
                print(self.frame_cnt, end='\r')
        return self.curr_frame

    def load_to_time(self, time, monitor=True):
        return self.load_to_frame(int(time*self.fps), monitor)

    def close(self):
        self.cap.release()

