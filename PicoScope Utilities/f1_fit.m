function [fit_res] = f1_fit(t,x)
%Fit x, t to x = A cos(w*t + phi) + c
%Gives parameter estimates in a struct
    f = fit(t, x,'fourier1');
    fit_res.c = f.a0;
    fit_res.A = sqrt(f.a1^2 + f.b1^2);
    fit_res.w = f.w;
    fit_res.phi = -atan2(f.b1, f.a1);
    bd = confint(f, 0.95);
    tt = tinv((1+0.95)/2, length(t));
    % https://www.mathworks.com/matlabcentral/answers/153547-how-can-i-compute-the-standard-error-for-coefficients-returned-from-curve-fitting-functions-in-the-c
    da0 = (bd(2,1) - bd(1,1))/(2*tt);
    da1 = (bd(2,2) - bd(1,2))/(2*tt);
    db1 = (bd(2,3) - bd(1,3))/(2*tt);
    dw = (bd(2,4) - bd(1,4))/(2*tt);
    dc = da0;
    dA = sqrt((da1/fit_res.A)^2 + (db1/fit_res.A)^2);
    pref = 1/(1 + f.b1^2/f.a1^2);
    dphi = sqrt((pref*da1)^2 + (pref*db1)^2);
    % should probably consider the intrinsic difference as well
    dt = t(2) - t(1);
    dphi_min = f.w * dt; % 2 * smallest division
    dV = min(nonzeros(abs(x(1:length(x)-1) - x(2:length(x)))));
    disp(dV);
    fit_res.dA = max(dA, dV);
    fit_res.dphi = max(dphi, dphi_min);
    fit_res.dc = max(dc, dV);
    fit_res.dw = dw;
end

