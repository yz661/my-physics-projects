from image_proc_lib.image_proc import *


class ImageAnalyser(object):
    def __init__(self, cv_cap):
        self.cap = cv_cap
        self.max_len_list = []
        self.frame_descriptor_list = []
        self.skip_n = 0
        self.pause_n = 1

        self.window_title = "set thresh"
        self.track_bar_name = "threshold value"

        self.curr_frame = None
        self.curr_frame_descriptor = None
        self.curr_roi = None
        self.background_img = None

        self.curr_mask = None
        self.use_mask = None
        self.mask_color = (0, 255, 0)
        self.mask_dilation = 5
        self.mask_dilation_iter = 2
        self.wait_time = 10

    def set_wait_time(self, wait_time):
        self.wait_time = wait_time

    def set_mask_dilation_param(self, dilation=5, dilation_iter=2):
        self.mask_dilation_iter = dilation_iter
        self.mask_dilation = dilation

    def set_mask_color(self, color):
        self.mask_color = color

    def set_background(self, background_img):
        self.background_img = background_img

    def set_skip(self, skip_n):
        self.skip_n = skip_n

    def set_pause_n(self, pause_n):
        self.pause_n = pause_n

    def clear(self):
        self.max_len_list = []
        self.frame_descriptor_list = []

    def move_to_next_frame(self):
        # complete the relevant fields in frame descriptor
        self.curr_frame_descriptor.roi = self.curr_roi
        self.curr_frame_descriptor.frame_ind_abs = self.cap.frame_cnt

        # record current frame information to the list
        curr_thresh_val = cv2.getTrackbarPos(self.track_bar_name, self.window_title)
        self.max_len_list.append([self.cap.frame_cnt, self.curr_frame_descriptor.max_len, curr_thresh_val])
        self.frame_descriptor_list.append(self.curr_frame_descriptor)

        # update the global mask
        # print(self.curr_frame_descriptor.get_convex_hull_rel())
        if self.use_mask:
            self.curr_mask = generate_mask(self.curr_frame,
                                           self.curr_roi,
                                           self.curr_frame_descriptor.get_convex_hull_rel(),
                                           dilation_size=self.mask_dilation,
                                           dilation_iter=self.mask_dilation_iter)

        # update the roi
        c = self.curr_frame_descriptor.get_convex_hull_rel()
        rect = cv2.boundingRect(c)
        rect_center = [rect[0] + rect[2] // 2, rect[1] + rect[3] // 2]
        new_roi_center = [self.curr_roi[0] + rect_center[0],
                          self.curr_roi[1] + rect_center[1]]
        exp_ratio = 1.5
        new_roi = [new_roi_center[0] - int(rect[2] * exp_ratio) // 2,
                   new_roi_center[1] - int(rect[3] * exp_ratio) // 2,
                   rect[2] * exp_ratio,
                   rect[3] * exp_ratio]
        self.curr_roi = new_roi

        # retrieve a new frame
        self.curr_frame = self.cap.retrieve_frame(skip_n=self.skip_n)
        # run the callback function to display the new image
        self.on_track_bar(curr_thresh_val)

    def on_track_bar(self, val):
        if not self.use_mask:
            self.curr_mask = None
        self.curr_frame_descriptor = analyse_img2(self.curr_frame,
                                                  self.background_img,
                                                  self.curr_roi,
                                                  val,
                                                  mask=self.curr_mask)
        self.curr_frame_descriptor.roi = self.curr_roi
        self.curr_frame_descriptor.frame_ind_abs = self.cap.frame_cnt
        highlighted_frame = self.curr_frame
        if self.use_mask and not (self.curr_mask is None):
            highlighted_frame = highlight_mask(self.curr_frame, self.curr_mask, color=self.mask_color)
        annotated_frame = annotate_frame_contour(highlighted_frame, self.curr_frame_descriptor)
        cv2.imshow(self.window_title, annotated_frame)

    def run(self, initial_roi, background_img):
        # initialize relevant information
        self.curr_roi = initial_roi
        self.set_background(background_img)

        if self.curr_frame is None:
            self.curr_frame = self.cap.retrieve_frame()
        cropped_frame = get_roi(self.curr_frame, self.curr_roi)

        # create the window for display
        cv2.namedWindow(self.window_title, flags=cv2.WINDOW_GUI_NORMAL + cv2.WINDOW_AUTOSIZE)
        track_bar_callback = lambda val: self.on_track_bar(val)
        cv2.createTrackbar(self.track_bar_name, self.window_title, 10, 100, track_bar_callback)
        # display the initial image
        self.on_track_bar(10)

        is_continuous = False
        while True:
            if is_continuous:
                c = cv2.waitKey(self.wait_time)
            else:
                c = cv2.waitKey(0)
            # check if exit
            if c == ord('q'):
                break
            # check if switch mode
            if is_continuous:
                if c > 0:
                    print("stop continuous mode     ")
                    is_continuous = False
            else:
                if c == ord('c'):
                    print("start continuous mode     ")
                    is_continuous = True
            # temp! Not protected!
            # for i in range(self.pause_n):
            #     self.move_to_next_frame()

            # go through a frame
            try:
                for i in range(self.pause_n):
                    self.move_to_next_frame()
            except:
                print("Something wrong!!! HALT !!!")
                cv2.destroyAllWindows()
                raise # throw the exception again to see what goes wrong from the terminal
                # break

            # conduct a check
            if len(self.max_len_list) > 2:
                curr_diameter = self.max_len_list[-1][1]
                last_diameter = self.max_len_list[-2][1]
                if abs(curr_diameter - last_diameter) / last_diameter > 0.1:
                    if is_continuous:
                        print("Change in diameter is too large, end continuous mode!")
                        is_continuous = False
            print("curr frame cnt = " + str(self.cap.frame_cnt), end="\r")
        cv2.destroyAllWindows()


def analyse_img2(target, background, roi, thresh_val, min_area=20, display=False, mask=None):
    target_cropped = None
    background_roi = None
    if mask is None:
        target_cropped = get_roi(target, roi)
        background_roi = get_roi(background, roi)
    else:
        target_cropped = get_masked_roi(target, mask, roi)
        background_roi = get_masked_roi(background, mask, roi)
    diff = target_cropped.astype(int) - background_roi.astype(int)
    diff = np.abs(diff)
    diff_gray = cv2.cvtColor(diff.astype(np.uint8), cv2.COLOR_BGR2GRAY)
    if display:
        disp_cv_img(diff_gray)
    ret, thresh1 = cv2.threshold(diff_gray,
                                 thresh_val,
                                 255,
                                 cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh1,
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)
    # filter the contours based on contour area
    selected_list = []
    for c in contours:
        if cv2.contourArea(c) >= min_area:
            selected_list.append(c)
    # extract all the contour points and run convex hull
    all_pts = []
    for c in selected_list:
        for pt in c:
            all_pts.append(pt)
    hull = cv2.convexHull(np.array(all_pts).astype(np.int32), False)
    descriptor = FrameDescriptor()
    descriptor.roi = roi
    # not really the largest contour,
    # but the drawing function should be ok with it
    descriptor.largest_contour_rel = selected_list # contours
    descriptor.convex_hull_rel = hull
    extreme_pts = find_extreme_pts(hull)
    descriptor.extreme_pt1 = extreme_pts[0]
    descriptor.extreme_pt2 = extreme_pts[1]
    descriptor.max_len = extreme_pts[2]
    return descriptor


def find_extreme_pts(hull):
    max_len = 0
    extreme_pt1 = None
    extreme_pt2 = None
    for i in range(len(hull)):
        for j in range(i + 1, len(hull)):
            vec = hull[i][0] - hull[j][0]
            dist = np.sqrt(vec[0] * vec[0] + vec[1] * vec[1])
            if dist > max_len:
                max_len = dist
                extreme_pt1 = hull[i][0]
                extreme_pt2 = hull[j][0]
    return extreme_pt1, extreme_pt2, max_len


def generate_mask(target, roi, hull_rel, dilation_size=5, dilation_iter=2):
    img_shape_2D = (target.shape[0], target.shape[1])
    blank = np.zeros(img_shape_2D, np.uint8)
    # convert relative coordinates to global ones
    polygon_pt_list = np.array([pt + [int(roi[0]), int(roi[1])] for pt in hull_rel])
    global_mask = cv2.fillPoly(blank, [polygon_pt_list], 255)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (dilation_size, dilation_size))
    global_mask = cv2.dilate(global_mask, kernel, iterations=dilation_iter)
    return global_mask


def get_masked_roi(target, global_mask, roi):
    masked_img_full = cv2.bitwise_or(target, target, mask=global_mask)
    return get_roi(masked_img_full, roi)


def highlight_mask(target, global_mask, color=(0, 255, 0), intensity=0.3):
    highlight = np.zeros(target.shape, np.uint8)
    highlight[:] = color
    highlight_masked = cv2.bitwise_or(highlight, highlight, mask=255 - global_mask)
    # disp_cv_img(highlight_masked)
    highlighted_img = cv2.addWeighted(target, 1, highlight_masked, 0.3, 0)
    return highlighted_img
