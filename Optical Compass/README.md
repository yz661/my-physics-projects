# Optical Compass

## Overview
This project was motivated by a question in the International Physicist's Tournament (IPT) 2020, where we were tasked to build a compass using polarization effect. 

Sky light from different angles is polarized to various degree depending on the position of the sun. The degree of linear polarization can be modelled by considering the Rayleigh scattering of direct sun light in the atmosphere. In this project, we aimmed to verify the proposed theory [1] of the degree of linear polarization in the sky light using an optical setup centered around a calibrated Raspberry pi v1 camera. The setup is able to make quantitative measurements of the degree of linear polarization in the sky light, and we have achieved qualitative agreement between the model and experiments. The application of the theory to the development of an optical compass using sky light polarization can be explored.

![Sample Result](https://gitlab.com/yz661/my-physics-projects/-/blob/master/Optical%20Compass/example%20data/sample_image.PNG)

## Content
- `camera calibration` contains information relevant for calibrating a raspberry pi v1 camera.
- `example data` contains example data for sky light polarization measurements.
- `experimental` contains information relevant to setting up the experimental.
- `Optical_Compass_Presentation.pdf` is a short presentation for the project.


## References
[1] Smith, G. S. (2007). The polarization of skylight: An example from nature. American Journal of Physics, 75(1), 25-35.
