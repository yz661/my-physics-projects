function x_3dB = est_3dB_pt(x, y, ref_val)
    [x_s, order] = sort(x);
    y_s = y(order);
    y_ref = max(y);
    if ~isnan(ref_val)
        y_ref = ref_val;
    end
    y_ref = y_ref - 0.15;
    x_3dB = [];
    for i=1:(length(x)-1)
        if (y_s(i) - y_ref)* (y_s(i+1) - y_ref) < 0
            grad = (y_s(i+1) - y_s(i))/(x_s(i+1) - x_s(i));
            x_pos = x_s(i) + (y_ref - y_s(i))/grad;
            x_3dB = [x_3dB, x_pos];
        end
    end
    x_3dB = 10^x_3dB;
end

