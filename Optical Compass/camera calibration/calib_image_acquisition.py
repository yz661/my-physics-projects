from time import sleep
from picamera import PiCamera

camera = PiCamera(resolution=(1280, 720), framerate=30)
# Set ISO to the desired value
camera.iso = 100
# Wait for the automatic gain control to settle
sleep(2)
# Now fix the values
camera.shutter_speed = camera.exposure_speed
camera.exposure_mode = 'off'
g = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = (1.28, 1.57)  # g
print(camera.exposure_speed)
# Finally, take several photos with the fixed settings
exp_list = []
file_name = []

camera.shutter_speed = 10000
exp = 10000
previous_exp = 0

for i in range(20):
    # capture at current exposure speed
    curr_exp = camera.exposure_speed
    print(camera.exposure_speed)
    if not(curr_exp == previous_exp):
        previous_exp = curr_exp
        camera.capture(str(curr_exp) + '.jpg')
        exp_list.append(curr_exp)
        file_name.append(str(curr_exp) + '.jpg')

    # reduce exposure speed
    exp = int(round(exp/1.2))
    camera.shutter_speed = exp
    camera.capture('o.jpg')
    print('intended = ' + str(exp) + ', actual = ' + str(camera.exposure_speed))

f = open("fileNames.csv", "w")
for i in range(len(exp_list)):
    f.write(str(file_name[i]) + ', ' + str(exp_list[i]) + '\r\n')
f.close()
