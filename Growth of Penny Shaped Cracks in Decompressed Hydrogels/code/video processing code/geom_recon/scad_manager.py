import subprocess
from stl import mesh
import stl
from image_proc_lib.file_util import *


class ScadManager(object):
    # need to define the extrusion height
    def __init__(self, h):
        self.h = h

    def code_gen(self, processed_descriptor, adjustment=None, angle_offset=0):
        dx = 0
        dy = 0
        angle = processed_descriptor.angle
        label = processed_descriptor.label
        if adjustment is not None:
            dx = adjustment[0]
            dy = adjustment[1]
        line1 = 'module extrude' + label + '(){\n'
        line2 = 'points = ['
        cnt = processed_descriptor.processed_cnt
        for pt in cnt:
            # remember to flip y coord
            line2 = line2 + '[' + "{0:.4g}".format(pt[0] + dx) + ',' + "{0:.4g}".format(pt[1] + dy) + ']' + ','
        line2 = line2[:-1] + '];\n'
        # this is a tiny additional rotation to deal with round off error...
        # http://forum.openscad.org/stl-not-correct-by-mesh-generators-td21241.html
        line3 = 'rotate([0, ' + str(angle - angle_offset + 0.1) + ', 0])\n'
        line3 = line3 + 'linear_extrude(height=h, center=true) polygon(points);\n'
        line4 = '}\n\n'
        return line1 + line2 + line3 + line4

    def generate_full_code(self, processed_descriptor_list, adjustment_list):
        st_dec = '$fa = 1;\n$fs = 0.4\n;$fn=10;\n;'
        h_def = 'h = ' + str(self.h) + ';\n\n'
        bodies = ''
        for i in range(len(processed_descriptor_list)):
            bodies = bodies + self.code_gen(processed_descriptor_list[i], adjustment_list[i])
        intersect_cmd = ''
        if len(processed_descriptor_list) == 1:
            intersect_cmd = 'extrude' + processed_descriptor_list[0].label + '();\n'
        else:
            intersect_cmd = 'intersection(){\n'
            for i in range(len(processed_descriptor_list)):
                intersect_cmd = intersect_cmd + 'extrude' + \
                                processed_descriptor_list[i].label + '();\n'
            intersect_cmd = intersect_cmd + '};'
        return h_def + bodies + intersect_cmd

    # intersect first n-1th body
    # leave the nth one, and rotate so that the nth one is facing the top
    # up_to_n is not the index!
    def generate_code_highlight(self, processed_descriptor_list, adjustment_list, up_to_n):
        if up_to_n == 1:
            print("generate_code_highlight must have at least two contours!")
            return ''
        # calculate the offset angle so the last body is facing up
        offset_angle = processed_descriptor_list[up_to_n - 1].angle

        st_dec = '$fa = 1;\n$fs = 0.4\n;$fn=10;\n;'
        h_def = 'h = ' + str(self.h) + ';\n\n'
        bodies = ''
        for i in range(up_to_n):
            bodies = bodies + self.code_gen(processed_descriptor_list[i], adjustment_list[i], offset_angle)
        intersect_cmd = 'intersection(){\n'
        for i in range(up_to_n - 1):
            intersect_cmd = intersect_cmd + 'extrude' + \
                            processed_descriptor_list[i].label + '();\n'
        intersect_cmd = intersect_cmd + '};\n\n'
        focused_body = 'color([1, 0, 0, 0.3])\n'
        focused_body = focused_body + 'extrude' + processed_descriptor_list[up_to_n - 1].label + '();\n'
        return h_def + bodies + intersect_cmd + focused_body

    def render_highlight(self, processed_descriptor_list, adjustment_list, up_to_n, file_name=None, label='test'):
        scad_code = self.generate_code_highlight(processed_descriptor_list, adjustment_list, up_to_n)
        if file_name is None:
            file_name = 'render ' + str(up_to_n) + ' ' + label
        else:
            file_name = file_name + ' ' + str(up_to_n) + ' ' + label
        scad_file_name = file_name + '.scad'
        png_file_name = file_name + '.png'
        cmd = 'openscad \"' + scad_file_name + '\" -o \"' + png_file_name + \
              '\" --preview --autocenter --projection o --camera 0,0,10,0,0,0 --viewall -q'
        with open(scad_file_name, 'w') as f:
            f.write(scad_code)
        subprocess.run(cmd, capture_output=True, shell=True)
        # print(cmd)

    # there cannot be any space in the label!
    def compute_intersection_volume(self, processed_descriptor_list, adjustment_list, up_to_n, label='0'):
        # generate the code
        full_code = self.generate_full_code(processed_descriptor_list[0:up_to_n],
                                            adjustment_list[0:up_to_n])
        # export to csv
        temp_file_name = 'temp' + label
        scad_file_name = temp_file_name + '.scad'
        stl_file_name = temp_file_name + '.stl'
        cmd = 'openscad -o ' + stl_file_name + ' ' + scad_file_name
        with open(scad_file_name, 'w') as f:
            f.write(full_code)
        subprocess.run(cmd, capture_output=True, shell=True)
        curr_intersection = mesh.Mesh.from_file(stl_file_name)
        volume, _, _ = curr_intersection.get_mass_properties()
        return volume

    def cal_vol_adjust(self, processed_descriptor_list, adjustment_list, adjusted_pos, adjustment):
        n = adjusted_pos + 1
        curr_descriptor_list = processed_descriptor_list[0:n]
        curr_adjustment_list = adjustment_list[0:n].copy()
        curr_adjustment_list[-1] = curr_adjustment_list[-1] + adjustment
        return self.compute_intersection_volume(curr_descriptor_list,
                                                curr_adjustment_list,
                                                n)

    def export_geom(self, processed_descriptor_list, adjustment_list, file_name):
        scad_code = self.generate_full_code(processed_descriptor_list, adjustment_list)
        scad_file_name = file_name + '.scad'
        stl_file_name = file_name + '.stl'
        f = open_file_w_with_check(scad_file_name, mode='w')
        if not (f is None):
            f.write(scad_code)
            f.close()
        cmd = 'openscad -o \"' + stl_file_name + '\" \"' + scad_file_name + '\"'
        subprocess.run(cmd, capture_output=True, shell=True)
        curr_intersection = mesh.Mesh.from_file(stl_file_name)
        volume, _, _ = curr_intersection.get_mass_properties()
        return volume
