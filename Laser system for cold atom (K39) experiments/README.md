# Laser system for cold atom (K39) experiments

## Overview
This is the project I did in Prof Zoran Hadzibabic's group over the summer of 2021. The group focuses on the experimental study of Bose-Einstein condensates (BEC). A new setup for K39 BEC was underconstruction when I joined the group for the summer. In the new setup, the temperature of the K39 atoms will be first reduced using laser cooling (Doppler cooling and Gray Molasses), and BEC can be achieved after evaporative cooling. Once BEC is achieved, further experimental studies can happen. The target of my experimental project is to construct the laser system to supply the laser frequencies needed for the laser cooling process.

The final report is given in this repo, and the abstract of the report is given below. Key experimental design figures can be found in the report.

## Abstract
Specific laser frequencies are needed for the laser cooling of atoms. This project aims to construct the laser system for the cooling of K39 atoms. In particular, the setup to supply the laser light for the 2D MOT, the 3D MOT, and the Gray Molasses cooling has been designed and partially constructed. In this report, we present our experimental designs and identify some of the main experimental challenges.

