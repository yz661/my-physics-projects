import numpy as np
from geom_recon.scad_manager import *


class AdjustmentCalculator(object):
    def __init__(self, scad_manager):
        self.scad_manager = scad_manager
        self.verbose = True
        self.cap = 1  # max change per step
        self.step_control = 0.005  # 0.5 % change is worth 1 pixel
        self.max_step = 10

    # calculate the adjustment for contour up to index curr_n
    # the rest of the given contours are ignored
    def find_adjustment(self, processed_descriptor_list, previous_adjustment_list, curr_n, adj_h, adj_v):
        curr_adjustment = np.array([0, 0])
        # start the iteration
        for i in range(self.max_step):
            x_change = 0
            y_change = 0
            curr_vol = self.scad_manager.cal_vol_adjust(processed_descriptor_list,
                                                        previous_adjustment_list,
                                                        curr_n,
                                                        curr_adjustment)
            add_adjustent = np.array([0, 0])
            # calculate x change
            if adj_h:
                vol_x_p = self.scad_manager.cal_vol_adjust(processed_descriptor_list,
                                                           previous_adjustment_list,
                                                           curr_n,
                                                           curr_adjustment + np.array([1, 0]))
                # percentage change in x direction
                x_change = (vol_x_p - curr_vol) / curr_vol

                add_x_adjustment = np.clip(x_change / self.step_control, -self.cap, self.cap)
                if abs(add_x_adjustment) < 1:
                    add_x_adjustment = 0
                add_adjustent[0] = add_x_adjustment

            # calculate y change
            if adj_v:
                vol_y_p = self.scad_manager.cal_vol_adjust(processed_descriptor_list,
                                                           previous_adjustment_list,
                                                           curr_n,
                                                           curr_adjustment + np.array([0, 1]))
                # percentage change in y direction
                y_change = (vol_y_p - curr_vol) / curr_vol

                add_y_adjustment = np.clip(y_change / self.step_control, -self.cap, self.cap)
                if abs(add_y_adjustment) < 1:
                    add_y_adjustment = 0
                add_adjustent[1] = add_y_adjustment

            # update curr_adjustment
            curr_adjustment = curr_adjustment + add_adjustent
            # print out all the information
            if self.verbose:
                print("iter: " + str(i))
                print("curr_vol: " + "{0:.5g}".format(curr_vol))
                if adj_h:
                    print("x change: " + "{0:.3g}".format(x_change * 100) + "%/pixel")
                if adj_v:
                    print("y change: " + "{0:.3g}".format(y_change * 100) + "%/pixel")
                print("additional_adjustment: " + str(add_adjustent))
                print('curr_adjustment: ' + str(curr_adjustment))
                print('')
            # check the stopping condition
            if np.all(add_adjustent == 0):
                print("adjustmnet ended")
                break
        return curr_adjustment

    # compute the adjustment needed for all
    def find_adjustment_all(self, processed_descriptor_list):
        n_elements = len(processed_descriptor_list)
        adjustments = np.zeros((n_elements, 2))
        for i in range(1, n_elements):
            if self.verbose:
                print("Adjusting for element " + str(i))
                print("=========================================================================")
            adj_h = True
            adj_v = True
            if i == 1:
                adj_h = False
            curr_adj = self.find_adjustment(processed_descriptor_list, adjustments, i, adj_h, adj_v)
            adjustments[i] = curr_adj
            if self.verbose:
                print('-------------------------------------------------------------------------')
                print("current_adjustment: " + str(adjustments.tolist()))
                print("=========================================================================")
                print('')
        return adjustments
