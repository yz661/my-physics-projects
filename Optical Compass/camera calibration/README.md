# Camera Calibration
This folder contains the code to calibrate a raspberry pi v1 camera. 

## Required
- Raspberry pi with a v1 camera.
- `picamera` on Raspberry pi.
- `Jupyter Notebook` on PC with `Wolfram Engine` (see [here](https://github.com/WolframResearch/WolframLanguageForJupyter))

## Content
- `calib_image_acquisition.py` is a Python file to be run on a Raspberry pi. This file will capture and export a series of images at different exposure times.
- `CameraCalibration.ipynb` is a Jupyter Notebook using Wolfram Engine. This file is used to process the acquired images. 

## Instructions
- Create a consistent and diffused light source.
- Point the camera on the Raspberry pi to the light source and run `calib_image_acquisition.py`.
- Transfer all the files exported by `calib_image_acquisition.py` to a PC. This includes a series of image files and a csv file containing the file names.
- Process the transferred files on a PC using `CameraCalibration.ipynb`. Details on how to use this Notebook is given as comments inside the Notebook

## Example
`example data.zip` is a set of example data.