# The application of cross-linked carbon nanotube and molybdenum-disulphide in energy storage

## Overview
This project was done in 2017 under the supervision of Dr Derrick Fam at Institute of Materials Research and Engineering (IMRE) in Singapore.

## Abstract
Cross-linked carbon nanotube (CNT) and molybdenum-disulphide (MoS2) composite was synthesized and 
characterized in this study. The electrochemical performance of the material in 1-Ethyl-3-methylimidazolium
bis(trifluoromethylsulfonyl)imide (EMI TFSI), 1 M sulfuric acid, and 0.5 M sodium sulphate was
electrochemically characterized in both three-electrode cell and two-electrode cell settings. Cyclic voltammetry
(CV), electrochemical impedance spectroscopy (EIS), and galvanostatic charge-discharge (GCD) were performed.
The introduction of MoS2 into the composite gives superior performance, particularly in organic and neutral
electrolyte. Specific capacitance of 19 F/g and 21F/g in CNT/MoS2 composite electrodes has been recorded in
EMI TFSI and in 0.5 M sodium sulphate respectively. Significant factors limiting the performance of the material,
namely, the presence of iron impurities and the large interfacial impedance between the free-standing electrode
and current collector, were identified. The study also indicates future direction on the optimization of device
performance, including the suppression of iron redox reaction, the tuning of MoS2 content and electrolyte
concentration, and the reduction of interfacial impedance.

![quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/The%20application%20of%20cross-linked%20CNT%20and%20MoS2%20in%20energy%20storage/quick_illus.PNG)

## Content
My final report as well as my presentation for the project are included in this folder