from matplotlib import pyplot as plt
import matplotlib
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


def plot_hull(hull_list, is_img_coord=True, is_show=True):
    flip = 1
    if is_img_coord:
        flip = -1
    n_hull = hull_list.shape[0]
    for i in range(n_hull):
        curr_hull = hull_list[i]
        x = curr_hull[:, 0]
        y = flip * curr_hull[:, 1]  # need to account for opencv difference
        plt.plot(np.append(x, x[0]), np.append(y, y[0]))
    ax = plt.gca()
    ax.set_aspect('equal', 'box')
    if is_show:
        plt.show()


def plot_circle(center, rad, is_show=True):
    n_pts = 100
    angle = np.linspace(0, 2 * np.pi, num=n_pts)
    x = np.cos(angle) * rad + center[0]
    y = np.sin(angle) * rad + center[1]
    plt.plot(np.append(x, x[0]), np.append(y, y[0]))
    ax = plt.gca()
    ax.set_aspect('equal', 'box')
    if is_show:
        plt.show()


def plot_processed_view(processed_descriptor_list, legends, adjustments=None):
    if not (len(processed_descriptor_list) == len(legends)):
        print("Length not matched!")
        return
    rad_list = [element.bead_rad for element in processed_descriptor_list]
    for i in range(len(processed_descriptor_list)):
        cnt = processed_descriptor_list[i].processed_cnt
        if adjustments is None:
            plot_hull(np.array([cnt]), is_img_coord=False, is_show=False)
        else:
            # negative as we are dealing with image coord rather than corrected coord
            adjusted_cnt = np.array(cnt) + adjustments[i]
            plot_hull(np.array([adjusted_cnt]), is_img_coord=False, is_show=False)
    plot_circle([0, 0], np.mean(np.array(rad_list)), is_show=False)
    plt.legend(legends + ['circ'])
    plt.show()


def plot_raw_hulls(frame_descriptors, legends=None):
    raw_hull_list = []
    for element in frame_descriptors:
        raw_hull_list.append(element.get_convex_hull_rel()[:, 0])
    plot_hull(np.array(raw_hull_list), is_show=False)
    plt.legend(legends)
    plt.show()


def plot_xy(xy, ax=None, x_offset=0, y_offset=0):
    if ax is None:
        ax = plt.gca()
        ax.set_aspect('equal', 'box')
    x = xy[:, 0] + x_offset
    y = xy[:, 1] + y_offset
    plt.plot(x, y)


def plot_ellipse_fit_subplot(ellipses, p_descriptors):
    fig = plt.figure(figsize=[10, 5])
    n_view = len(ellipses)
    for i in range(n_view):
        ax = fig.add_subplot(1, n_view, i+1)
        ax.set_aspect('equal', 'box')
        plot_xy(p_descriptors[i].processed_cnt, ax=ax)
        ellipses[i].plot(ax=ax)
    # plt.show()


def get_cnt_min_max(cnt):
    x = cnt[:, 0]
    y = cnt[:, 1]
    return [min(x), max(x), min(y), max(y)]


def plot_ellipse_fit_single_plot(ellipses, p_descriptors):
    matplotlib.rcParams['axes.prop_cycle'] = matplotlib.cycler(color=['#1f77b4', '#ff7f0e'])
    n_view = len(ellipses)
    fig = plt.figure(figsize=[10, 5])
    ax = plt.gca()
    ax.set_aspect('equal', 'box')
    last_x_pos = 0
    for i in range(n_view):
        x_min, x_max, _, _ = get_cnt_min_max(p_descriptors[i].processed_cnt)
        x_offset = last_x_pos - x_min + 10
        last_x_pos = x_offset + x_max
        plot_xy(p_descriptors[i].processed_cnt, ax=ax, x_offset=x_offset)
        ellipses[i].plot(ax=ax, x_offset=x_offset)
    # plt.show()
    # put things back
    matplotlib.rcParams['axes.prop_cycle'] = matplotlib.cycler(color=['#1f77b4', '#ff7f0e',
                                                                      '#2ca02c', '#d62728',
                                                                      '#9467bd', '#8c564b',
                                                                      '#e377c2', '#7f7f7f',
                                                                      '#bcbd22', '#17becf'])


# this thing is usable but slightly buggy at the moment
class Plotter3D(object):
    def __init__(self):
        self.fig = None
        self.angles = [0, 45, 90, 135]
        self.axes = []
        self.init_axes()
        return

    def init_axes(self):
        # initialize all the axis
        self.fig = plt.figure(figsize=(8, 8))
        for i in range(4):
            self.axes.append(self.fig.add_subplot(2, 2, i + 1, projection='3d'))
        return

    # takes in points as column vectors
    def add_points(self, pts, size=20, alpha=1):
        dim = pts.shape[0]
        if not (dim == 3):
            print("Error, column vectors are expected!")
            return
        x = pts[0]
        y = pts[1]
        z = pts[2]
        for ax in self.axes:
            ax.scatter(x, y, z, s=size, alpha=alpha)
        return

    def show(self, ele=30):
        for i in range(len(self.axes)):
            self.axes[i].view_init(ele, self.angles[i])
        plt.show()


