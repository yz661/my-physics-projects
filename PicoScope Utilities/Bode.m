load('f1_fit_export\pre_proc.mat');
bode_export_dir = 'Bode export';
i_c = 1;
o_c = 2;
use_input_freq = 0;
V_fixed_perc_err = 0.00;
f_fixed_perc_err = 0.05;
plot_title = 'Bode test';

mkdir(bode_export_dir)
n_pt = length(file_list);

% unpack!
Vamp_list = zeros(n_pt, 2);
dVamp_list = zeros(n_pt, 2);
Voff_list = zeros(n_pt, 2);
dVoff_list = zeros(n_pt, 2);
phase_list = zeros(n_pt, 2);
dphase_list = zeros(n_pt, 2);
freq_list = zeros(n_pt, 2);
dfreq_list = zeros(n_pt, 2);
for i=1:n_pt
    for j=1:2
        Vamp_list(i, j) = fit_list(i, j).A;
        dVamp_list(i, j) = fit_list(i, j).dA;
        Voff_list(i, j) = fit_list(i, j).c;
        dVoff_list(i, j) = fit_list(i, j).dc;
        phase_list(i, j) = fit_list(i, j).phi;
        dphase_list(i, j) = fit_list(i, j).dphi;
        freq_list(i, j) = fit_list(i, j).w/(2*pi);
        dfreq_list(i, j) = fit_list(i, j).dw/(2*pi);
    end
end

%Compute the freq. 
%This selects the better measurement from the channels
f_list = zeros(n_pt, 1);
df_list = zeros(n_pt, 1);
if use_input_freq == 1
    f_list = freq_list(:, i_c);
    df_list = dfreq_list(:, i_c);
else
    for i=1:n_pt
        if dfreq_list(i, 1) > dfreq_list(i, 2)
            f_list(i) = freq_list(i, 2);
            df_list = dfreq_list(i, 2);
        else
            f_list(i) = freq_list(i, 1);
            df_list = dfreq_list(i, 1);
        end
    end
end
lg_f = log10(f_list);
df_list = df_list + f_fixed_perc_err*f_list;
dlg_f = df_list./f_list;

% compute the gain
V_in = Vamp_list(:, i_c);
V_out = Vamp_list(:, o_c);
dV_in = dVamp_list(:, i_c) + V_fixed_perc_err* V_in;
dV_out = dVamp_list(:, o_c) + V_fixed_perc_err* V_out;
gain = V_out./V_in;
lg_gain = log10(gain);
dgain = gain.*sqrt((dV_in./V_in).^2 + (dV_out./V_out).^2);
dlg_gain = dgain./gain;

% compute the phase difference
phase_in = phase_list(:, i_c);
dphase_in = dphase_list(:, i_c);
phase_out = phase_list(:, o_c);
dphase_out = dphase_list(:, o_c);
phase_diff = phase_out - phase_in;
for i=1:length(phase_diff)
    if phase_diff(i) < 0
        phase_diff(i) = phase_diff(i) + 2*pi;
    end
end
dphase_diff = sqrt(dphase_in.^2 + dphase_out.^2);

% finally, the Bode plot
fig_gain = figure('Name','Bode plot (gain)');
errorbar(lg_f, lg_gain, dlg_gain, dlg_gain, dlg_f, dlg_f, '+');
set_ax_lim(lg_f, lg_gain, fig_gain);
xlabel('lg(f/Hz)');
ylabel('lg(gain)');
grid on
title(strcat(plot_title,' (gain)'));
export_img_file_name = strcat(bode_export_dir, '/', plot_title, ' (gain).png');
exportgraphics(fig_gain, export_img_file_name,'Resolution',300)
label_scatter_pt(lg_f, lg_gain, file_list, fig_gain);
export_img_file_name = strcat(bode_export_dir, '/', plot_title, ' (gain labelled).png');
exportgraphics(fig_gain, export_img_file_name,'Resolution',300)

fig_phase = figure('Name','Bode plot (phase)');
errorbar(lg_f, phase_diff, dphase_diff, dphase_diff, dlg_f, dlg_f, '+');
set_ax_lim(lg_f, phase_diff, fig_phase);
xlabel('lg(f/Hz)');
ylabel('phase difference/rad');
grid on
title(strcat(plot_title,' (phase)'));
export_img_file_name = strcat(bode_export_dir, '/', plot_title, ' (phase).png');
exportgraphics(fig_phase, export_img_file_name,'Resolution',300)
label_scatter_pt(lg_f, phase_diff, file_list, fig_phase);
export_img_file_name = strcat(bode_export_dir, '/', plot_title, ' (phase labelled).png');
exportgraphics(fig_gain, export_img_file_name,'Resolution',300)

disp("3dB point frequency (estimated)")
disp(est_3dB_pt(lg_f, lg_gain, 1));

raw_data = table(file_list', f_list, df_list, lg_f, dlg_f, ...
                 V_in, dV_in, V_out, dV_out, gain, dgain, ...
                 lg_gain, dlg_gain, phase_diff, dphase_diff,...
                 'VariableNames',... 
                 {'file_list', 'f(Hz)', 'err f', 'lg(f/Hz)', 'err lg(f)', ...
                 'V_in(V)', 'err V_in', 'V_out (V)', 'err V_out', 'gain', 'err gain', ...
                 'lg(gain)', 'err lg(gain)', 'phase_diff(rad)', 'err phase diff'});
export_data_file_name = strcat(bode_export_dir, '/', plot_title, ' raw data.csv');
writetable(raw_data, export_data_file_name, 'Delimiter', ',')  