# Growth of Gas-filled Penny-Shaped Cracks in Decompressed Hydrogels

## Overview
This project was done in the summer of 2020 under the supervision of Dr Lefauve and Dr Etzold from DAMTP.The project aimmed to investigate the potential growth of fracturing cracks in tissue as a result of decompression sickness.

## Abstract
We report that the decompression of soft brittle materials can lead to the growth of internal gas-filled cracks. These cracks are oblate spheroids ('penny shape'), whose main radius grows linearly in time, irreversibly fracturing the surrounding material. Our optical measurements in hydrogels characterise andquantify the three-dimensional crack geometry and growth rate. These results are in good agreementwith our analytical model coupling fracture mechanics and gas diffusion, and predicting the dependenceon the mechanical properties, gas diffusivity and super-saturation conditions (gas pressure, solubility, temperature).  Our results suggest a new potential mechanism for decompression sickness in scubadiving and for indirect optical measurements of the fracture properties of hydrogels.

![quick result summary](https://gitlab.com/yz661/my-physics-projects/-/blob/master/Growth%20of%20Penny%20Shaped%20Cracks%20in%20Decompressed%20Hydrogels/quick_image.PNG)

## Data Access
The data and codes associated with this paper can be freely down-loaded from the repository [here](doi.org/10.17863/CAM.60212).
The code and the 3D models for my experimental setup are also hosted in the current repository (see the subfolders).

## Content

- `code` folder contains the data analysis code developed in the project.

- `experimental setup` folders contains information related to my experimental setup.

- `presentation.pdf` gives a quick presentation of the project.