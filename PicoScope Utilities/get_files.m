function file_list = get_files(directory, ext)
% return the filenames (without ext) of files with
% a given extension in the directory as a cell array
    all_files = dir(directory);
    file_list = {};
    for i=1:length(all_files)
        curr_obj = all_files(i);
        if curr_obj.isdir
            continue
        end
        name_len = length(curr_obj.name);
        curr_ext = curr_obj.name(name_len-3:name_len);
        if strcmp(ext, curr_ext)
            file_list{end+1} = curr_obj.name(1:name_len-4);
        end
    end
end

