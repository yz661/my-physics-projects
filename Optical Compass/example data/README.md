This folder contains some example data for sky light polarization measurement.
- `27_Feb_2020.zip` contains the data measured outside the Cavendish Laboratory on 27 Feb 2020.
- `6_Mar_2020` contains the data measured outside the Cavendish Laboratory on 6 Mar 2020. The data is split into two zip files (to deal with the size limit problem).

Note that there seems to be a small error inside the `6_Mar_2020`. The camera might have been accidentally rotated by 180 degree. For more reliable camera alignment, please look at the `27_Feb_2020.zip` data.