from matplotlib import pyplot as plt
import numpy as np

from numpy.linalg import norm, eig, inv, lstsq
from numpy import sin, cos, pi, arctan, sqrt

# two reference:
# reconstructing ellipsoids from projections, William C. Karl Nov 10 1993
# application of moment and fourier descriptors to accurate estimation of elliptical shape parameters


class FourierEllipse(object):
    def __init__(self, hull=None, param=None):
        self.a0 = 0
        self.c0 = 0
        self.a1 = 0
        self.b1 = 0
        self.c1 = 0
        self.d1 = 0
        self.T = 0
        if hull is not None:
            [self.a0, b0, self.c0, d0, self.T] = compute_fourier_coeff(hull, 0)
            [self.a1, self.b1, self.c1, self.d1, T] = compute_fourier_coeff(hull, 1)
        elif param is not None:
            print("Not implemented!")

    def get_matrix(self):
        f = (self.a1 * self.d1 - self.b1 * self.c1)
        f = f * f
        m11 = (self.c1 * self.c1 + self.d1 * self.d1) / f
        m22 = (self.a1 * self.a1 + self.b1 * self.b1) / f
        m12 = -(self.a1 * self.c1 + self.b1 * self.d1) / f
        mat = np.array([[m11, m12], [m12, m22]])
        m_e = MatEllipse(mat=mat)
        m_e.offset(self.a0, self.c0)
        return m_e

    def plot(self, add_offset=True, ax=None, x_offset=0, y_offset=0):
        if ax is None:
            ax = plt.gca()
            ax.set_aspect('equal', 'box')
        t = [i * 2 * pi / 100 for i in range(101)]
        if add_offset:
            x = self.a1 * cos(t) + self.b1 * sin(t) + self.a0 + x_offset
            y = self.c1 * cos(t) + self.d1 * sin(t) + self.c0 + y_offset
            ax.plot(x, y)
        else:
            x = self.a1 * cos(t) + self.b1 * sin(t) + x_offset
            y = self.c1 * cos(t) + self.d1 * sin(t) + y_offset
            ax.plot(x, y)


class MatEllipse(object):
    def __init__(self, mat=None, hull=None):
        if mat is not None:
            self.mat = mat
        elif hull is not None:
            print("Not implemented")
        self.x_offset = 0
        self.y_offset = 0

    def offset(self, x, y):
        self.x_offset = x
        self.y_offset = y

    def plot(self, add_offset=True, ax=None, x_offset=0, y_offset=0):
        if ax is None:
            ax = plt.gca()
            ax.set_aspect('equal', 'box')
        e_vals, e_vecs = eig(self.mat)
        t_list = np.array([i * 2 * pi / 100 for i in range(101)])
        x_list = cos(t_list) * 1 / sqrt(e_vals[0])
        y_list = sin(t_list) * 1 / sqrt(e_vals[1])
        pts = np.array([x_list, y_list])
        pts = np.matmul(e_vecs, pts)
        if add_offset:
            ax.plot(pts[0] + self.x_offset + x_offset, pts[1] + self.y_offset + y_offset)
        else:
            ax.plot(pts[0] + x_offset, pts[1] + y_offset)

    def to_vec(self, invert=False):
        M = self.mat
        if invert:
            M = inv(M)
        return np.array([M[0, 0], M[0, 1], M[1, 1]])

    @staticmethod
    def vec2mat(v, invert=False):
        if not (len(v) == 3):
            print("Error, length not correct!")
        mat = np.array([[v[0], v[1]], [v[1], v[2]]])
        if invert:
            mat = inv(mat)
        return MatEllipse(mat=mat)


class MatEllipsoid(object):
    def __init__(self, mat, center=None):
        self.mat = np.array(mat)
        if center is None:
            center = np.array([0, 0, 0])
        self.center = np.array(center)

    # plot the projection to x-y plane by default if no rotation matrix is given
    # the rotation matrix rotates the points on the ellipse
    # if the view is to rotated, the angle needs to be reversed.
    def plot(self, rot=None, ax=None, x_offset=0, y_offset=0, color=(0, 1, 1, 0.3)):
        if ax is None:
            ax = plt.gca()
            ax.set_aspect('equal', 'box')
        e_val, e_vecs = eig(self.mat)
        theta_list = [i * np.pi / 100 for i in range(101)]
        phi_list = [i * 2 * np.pi / 100 for i in range(101)]
        plot_list = []
        for theta in theta_list:
            for phi in phi_list:
                x = sin(theta) * cos(phi) / sqrt(e_val[0])
                y = sin(theta) * sin(phi) / sqrt(e_val[1])
                z = cos(theta) / sqrt(e_val[2])
                plot_list.append([x, y, z])
        plot_list = np.transpose(plot_list)
        plot_list = np.matmul(e_vecs, plot_list)
        if rot is not None:
            plot_list = np.matmul(rot, plot_list)
        ax.scatter(plot_list[0] + x_offset,
                   plot_list[1] + y_offset,
                   color=color, alpha=0.5, s=1)

    # note that this returns column vectors!
    def get_plot_pts(self, centered=False, offset=(0, 0, 0)):
        if offset is None:
            offset = [0, 0, 0]
        if not centered:
            offset = np.array(offset) + np.array(self.center)
        e_val, e_vecs = eig(self.mat)
        theta_list = [i * np.pi / 100 for i in range(101)]
        phi_list = [i * 2 * np.pi / 100 for i in range(101)]
        plot_list = []
        for theta in theta_list:
            for phi in phi_list:
                x = sin(theta) * cos(phi) / sqrt(e_val[0])
                y = sin(theta) * sin(phi) / sqrt(e_val[1])
                z = cos(theta) / sqrt(e_val[2])
                plot_list.append([x, y, z])
        plot_list = np.transpose(plot_list)
        plot_list = np.matmul(e_vecs, plot_list)
        plot_list[0] = plot_list[0] + offset[0]
        plot_list[1] = plot_list[1] + offset[1]
        plot_list[2] = plot_list[2] + offset[2]
        return plot_list

    def get_vol(self):
        e_val, e_vecs = eig(self.mat)
        e_val = np.sqrt(e_val)
        return 4/3*np.pi/e_val[0]/e_val[1]/e_val[2]

    def ax_len(self):
        e_vals = np.sort(eig(self.mat)[0])
        return 1/np.sqrt(e_vals)

    # give a vector representation of itself
    def to_vec(self, invert=False):
        M = self.mat
        if invert:
            M = inv(M)
        return np.array([M[0, 0], M[0, 1], M[0, 2],
                         M[1, 1], M[1, 2], M[2, 2]])

    # map from (i, j) mat representation of a 3D positive definite
    # matrix to a vector representation
    # i, j are one based
    @staticmethod
    def cal_ind(i, j):
        ind_dict = {(1, 1): 0, (1, 2): 1, (1, 3): 2,
                    (2, 2): 3, (2, 3): 4, (3, 3): 5}
        if i > j:
            k = i
            i = j
            j = k
        return ind_dict[(i, j)]

    # construct a matrix representation from a vector presentation
    @staticmethod
    def vec2mat(v, invert=False):
        if not (len(v) == 6):
            print("Error, length not correct!")
        mat = np.array([[v[0], v[1], v[2]],
                        [v[1], v[3], v[4]],
                        [v[2], v[4], v[5]]])
        if invert:
            mat = inv(mat)
        return MatEllipsoid(mat)

    # compute a 3*7 matrix that projects the 3*3 positive definite mat
    # to a 2*2 positive definite mat
    # C is a 3*2 matrix whose columns are the x, y axis of the projection
    # plane in the world coordinate
    @staticmethod
    def cal_projection_rows(C):
        C = np.array(C)
        if not (C.shape == (3, 2)):
            print("Shape of C is not correct!")
        row11 = MatEllipsoid.cal_row(C, 1, 1)
        row12 = MatEllipsoid.cal_row(C, 1, 2)
        row22 = MatEllipsoid.cal_row(C, 2, 2)
        return np.array([row11, row12, row22])

    # helper method for the one above
    @staticmethod
    def cal_row(C, i, j):
        row = np.zeros(6)
        for k in range(1, 4):
            for l in range(1, 4):
                ind = MatEllipsoid.cal_ind(k, l)
                row[ind] = row[ind] + C[k - 1, i - 1] * C[l - 1, j - 1]
        return row

    # calculate the 2*2 mat representing the projection of the ellipsoid.
    # C is a 3*2 matrix whose columns are the x, y axis of the projection
    # plane in the world coordinate
    def project(self, C):
        projection_mat = MatEllipsoid.cal_projection_rows(C)
        vec_form3 = self.to_vec(invert=True)
        vec_form2 = np.matmul(projection_mat, vec_form3)
        mat_ellipse = MatEllipse.vec2mat(vec_form2, invert=True)
        return mat_ellipse


# the pt_list must be in the correct order
def compute_fourier_coeff(pt_list, n):
    pt_list = np.array(pt_list)
    pt_list = np.append(pt_list, [pt_list[0]], axis=0)
    t = [0]
    for i in range(1, len(pt_list)):
        curr_t = t[-1] + norm(pt_list[i] - pt_list[i-1])
        t.append(curr_t)
    T = t[-1]
    a = 0
    b = 0
    c = 0
    d = 0
    if n > 0:
        for i in range(1, len(pt_list)):
            dx = pt_list[i][0] - pt_list[i-1][0]
            dy = pt_list[i][1] - pt_list[i-1][1]
            dt = t[i] - t[i-1]
            pf = T/(2*n*n*pi*pi*dt)
            cosf = cos(2*n*pi*t[i]/T) - cos(2*n*pi*t[i-1]/T)
            sinf = sin(2*n*pi*t[i]/T) - sin(2*n*pi*t[i-1]/T)
            a = a + pf*dx*cosf
            b = b + pf*dx*sinf
            c = c + pf*dy*cosf
            d = d + pf*dy*sinf
    if n == 0:
        for i in range(1, len(pt_list)):
            dt = t[i] - t[i-1]
            a = a + 0.5*(pt_list[i][0] + pt_list[i-1][0])*dt/T
            c = c + 0.5*(pt_list[i][1] + pt_list[i-1][1])*dt/T
    return [a, b, c, d, T]

# find the relevant tests in IT20
