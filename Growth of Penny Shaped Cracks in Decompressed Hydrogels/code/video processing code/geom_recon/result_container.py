class EllipsoidFitSummary(object):
    def __init__(self, frame_list, ellipsoid_list, ref_ax):
        self.frame_list = frame_list
        self.ellipsoid_list = ellipsoid_list
        self.ref_ax = ref_ax


class ExtrusionEstimates(object):
    def __init__(self, frame_list, fit_list):
        self.frame_list = frame_list
        self.ellipsoid_fit_list = fit_list

