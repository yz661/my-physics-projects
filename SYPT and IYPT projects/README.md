# SYPT and IYPT

# Overview
This folder contains a list of the project I have done during my involvement with the Singapore Young Physicist's Tournament (SYPT) and the International Young Physicist Tournament (IYPT).

# Content
## Cusps in a Cylinder
This project was done by me during my preparation for SYPT 2018.

When a horizonatal cylinder is partially filled with a viscous fluid and rotated around its axis, a thin film of liquid will be pulled out from the liquid resevoir at the bottom due to the rotation of the wall. When the liquid film re-enters the bottom resevoir, a liquid front will be formed. Depending on the speed of the rotation, various instabilities could develop around the liquid front. In this project, the instabilites in this system (cusp formation and sloshing instability) were investigated both experimentally and theoretically. 2D steady state solutions to the problem was calculated using the finite difference method following [1] and validated against experiments. The growth of instabilities along the axial directly were analysed using linear stability analysis, and some qualitative insgihts were obtained through the analysis.

![Quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/cusp_illus.PNG)

My presentation for the project is included in this folder.

## Soy Sauce Optics
This project was done in collaboration with Max Tan whom I mentored for IYPT 2019.

When a laser beam passes through a thin layer of a highly absorbing liquid (e.g. 200 um of soy sauce), thermal lens effect could be observed as the heated liquid undegoes a change in refractive index. This phenomenon was investigated in this project both experimentally and theoretically. Experimentally, we have observed both the weak thermal lens effect (where the beam spot simply expands) and the strong thermal lens effect (where significant interference pattern is visible). A numerical transient theory was developed in the Fraunhofer regime and shown to be in very good agreement with our experimental results.

![Quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/soy_sauce_illus.PNG)

The presenation for this project may be requested from the contributors.


## Acoustic Levitation
This short project was motivated by a quation in IYPT 2018.

Acoustic standing waves are known to be able to create a trapping Gorkov potential, and this small objects to be levitated acoustically. However, recent developments suggests that a trapping Gorkov potential can be achieved without a standing wave. Following [2], I built a single sided phased array of ultrasounic transducers that is able to levitate small objects. The phased array was controlled by an Arduino mega (which was in turn controlled by a Python code on my PC). The setup was demonstrated to be working, and it was able to move the levitated small particle in 3D.

![Acoustic levitation in action](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/levitator_illus.PNG)

Some code and the setup information that are still retained can be found in `levitator.zip`. (Unfortunately, the circuit board design files I used were not archived.) Otherwise, there is an amazing video I took after the setup was built [here](https://drive.google.com/file/d/1Tpm9-7tknDXtD_6Y3QHdEolfOpLb6fCu/view?usp=sharing). The video was used by Jerry Han when he presented on the problem of acoustic levitation at IYPT2018.


[1] Hosoi, A. E., & Mahadevan, L. (1999). Axial instability of a free-surface front in a partially filled horizontal rotating cylinder. Physics of Fluids, 11(1), 97-106.

[2] Marzo, A., Seah, S. A., Drinkwater, B. W., Sahoo, D. R., Long, B., & Subramanian, S. (2015). Holographic acoustic elements for manipulation of levitated objects. Nature communications, 6, 8661.
