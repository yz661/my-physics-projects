function sample_rate = hantek_sample_rate(line)
%Maps clock to sample rate
    switch line
        case '#CLOCK=500uS'
            sample_rate = 1e6;
        case '#CLOCK=200uS'
            sample_rate = 1e6;
        case '#CLOCK=100uS'
            sample_rate = 1e6;
        case '#CLOCK=50.0uS'
            sample_rate = 1e6;
        case '#CLOCK=20.0uS'
            sample_rate = 4e6;
        case '#CLOCK=10.0uS'
            sample_rate = 8e6;
        case '#CLOCK=5.00uS'
            sample_rate = 16e6;
        case '#CLOCK=2.00uS'
            sample_rate = 48e6;
        case '#CLOCK=1.00uS'
            sample_rate = 48e6;
        case '#CLOCK=1.00mS'
            sample_rate = 1e6;
        case '#CLOCK=2.00mS'
            sample_rate = 1e6;
        case '#CLOCK=5.00mS'
            sample_rate = 1e6;
        case '#CLOCK=10.0mS'
            sample_rate = 1e6;
        case '#CLOCK=20.0mS'
            sample_rate = 1e6;
        case '#CLOCK=50.0mS'
            sample_rate = 1e6;
        case '#CLOCK=100mS'
            sample_rate = 1e6;
        case '#CLOCK=200mS'
            sample_rate = 500e3;
        otherwise
            disp(line);
            disp('Error, not recognized');
    end

end

