from image_proc_lib.cv_util import *
from image_proc_lib import json_util


# basically a container class to make life easier
class FrameDescriptor(object):
    def __init__(self):
        self.version = "1.0"
        self.largest_contour_rel = None
        self.convex_hull_rel = None  # relative to the ROI given
        self.roi = None
        self.frame_ind_abs = None  # start counting from the first frame
        self.extreme_pt1 = None
        self.extreme_pt2 = None
        self.max_len = None

    # access the members via getter only!
    def get_check_none(self, var_name):
        obj = self.__dict__[var_name]
        if obj is None:
            print("Attempt to retrieve " + var_name + " which is None!")
            raise ValueError
        return obj

    def get_largest_contour_rel(self):
        return self.get_check_none("largest_contour_rel")

    def get_convex_hull_rel(self):
        return self.get_check_none("convex_hull_rel")

    def get_roi(self):
        return self.get_check_none("roi")

    def get_frame_ind_abs(self):
        return self.get_check_none("frame_ind_abs")

    def get_extreme_pt1(self):
        return self.get_check_none("extreme_pt1")

    def get_extreme_pt2(self):
        return self.get_check_none("extreme_pt2")

    def get_max_len(self):
        return self.get_check_none("max_len")

    def check_items(self):
        for key in self.__dict__:
            if self.__dict__[key] is None:
                print(str(key) + " is None!")

    def __str__(self):
        return json_util.json_dumps(self)


def process_gray_frame(gray_frame, thresh_val=150, display_enabled=False):
    # simple threshold, not very robust at the moment
    ret, thresh1 = cv2.threshold(gray_frame, thresh_val, 255, cv2.THRESH_BINARY)
    if display_enabled:
        disp_cv_img(thresh1)

    # find the largest contour and perform a convex hull
    contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    c = max(contours, key=cv2.contourArea)
    hull = cv2.convexHull(c, False)

    # find the furthest two points on the contour
    # now using a slow and stupid method...
    max_len = 0
    extreme_pt1 = None
    extreme_pt2 = None
    for i in range(len(hull)):
        for j in range(i + 1, len(hull)):
            vec = hull[i][0] - hull[j][0]
            dist = np.sqrt(vec[0] * vec[0] + vec[1] * vec[1])
            if dist > max_len:
                max_len = dist
                extreme_pt1 = hull[i][0]
                extreme_pt2 = hull[j][0]
    if display_enabled:
        print(max_len)
        print(extreme_pt1)
        print(extreme_pt2)

    frame_descriptor = FrameDescriptor()
    frame_descriptor.largest_contour_rel = c
    frame_descriptor.convex_hull_rel = hull
    frame_descriptor.extreme_pt1 = extreme_pt1
    frame_descriptor.extreme_pt2 = extreme_pt2
    frame_descriptor.max_len = max_len
    return frame_descriptor  # (c, hull, max_len, extreme_pt1, extreme_pt2)


# do some drawing
def draw_length_bar(img, pt1, pt2, thickness):
    color = (0, 0, 255)
    cv2.line(img, tuple(pt1), tuple(pt2), color, thickness, lineType=cv2.LINE_AA)
    vec = pt1 - pt2
    p_dir = np.array([-vec[1], vec[0]])
    p_dir = p_dir // 10
    cv2.line(img, tuple(pt1), tuple(pt1 + p_dir), color, thickness, lineType=cv2.LINE_AA)
    cv2.line(img, tuple(pt1), tuple(pt1 - p_dir), color, thickness, lineType=cv2.LINE_AA)
    cv2.line(img, tuple(pt2), tuple(pt2 + p_dir), color, thickness, lineType=cv2.LINE_AA)
    cv2.line(img, tuple(pt2), tuple(pt2 - p_dir), color, thickness, lineType=cv2.LINE_AA)


def annotate_frame_contour(frame, frame_descriptor, max_size=600):
    # extract information from the frame descriptor
    if frame is None:
        print("annotate_frame_contour: frame is none!")
        return None

    c = frame_descriptor.get_largest_contour_rel()
    hull = frame_descriptor.get_convex_hull_rel()
    extreme_pt1 = frame_descriptor.get_extreme_pt1()
    extreme_pt2 = frame_descriptor.get_extreme_pt2()
    # crop the frame
    cropped_frame = get_roi(frame, frame_descriptor.get_roi())

    # draw contours
    frame_with_contour = cropped_frame.copy()
    cv2.drawContours(frame_with_contour, c, -1, (255, 255, 0), 1)
    cv2.drawContours(frame_with_contour, hull, -1, (255, 0, 255), 1)
    # resize
    frame_with_contour, disp_ratio = adjust_img_size(frame_with_contour, max_size)
    # draw length bar
    draw_length_bar(frame_with_contour,
                    np.array(extreme_pt1 * disp_ratio).astype(int),
                    np.array(extreme_pt2 * disp_ratio).astype(int),
                    int(disp_ratio))
    return make_square(frame_with_contour)  # pad the image to make it look nice