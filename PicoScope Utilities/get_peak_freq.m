function [f_peak, dBV_peak, f_res] = get_peak_freq(f,dBV)
% find the peaks in the spectrum plots
% remove the DC offset
    f_res = f(2) - f(1);
    f_peak = [];
    dBV_peak = [];
    n_drop = round(200/f_res);
    f = f(n_drop:length(f));
    dBV = dBV(n_drop:length(dBV));
    [dBV_s, order] = sort(dBV, 'descend');
    f_s = f(order);
    % The first 5 harmonics are taken.
    cnt = 0;
    cnt_required = 5;
    i = 1;
    while i < length(dBV_s)
       dBV_peak = [dBV_peak, dBV_s(i)]; 
       f_peak = [f_peak, f_s(i)];
       cnt = cnt + 1;
       if cnt == cnt_required
           break;
       end
       while min(abs(f_s(i) - f_peak)) < 5*f_res
           i = i + 1;
       end
    end
end

