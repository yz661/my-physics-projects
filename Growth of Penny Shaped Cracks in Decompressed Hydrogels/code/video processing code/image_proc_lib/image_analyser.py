from image_proc_lib.image_proc import *


class ImageAnalyser(object):
    def __init__(self, cv_cap):
        self.cap = cv_cap
        self.max_len_list = []
        self.frame_descriptor_list = []
        self.skip_n = 0
        self.pause_n = 1

        self.window_title = "set thresh"
        self.track_bar_name = "threshold value"

        self.current_frame = None
        self.current_gray_frame = None
        self.current_roi = None

    def set_skip(self, skip_n):
        self.skip_n = skip_n

    def set_pause_n(self, pause_n):
        self.pause_n = pause_n

    def clear(self):
        self.max_len_list = []
        self.frame_descriptor_list = []

    def move_to_next_frame(self):
        # process current frame first
        curr_thresh_val = cv2.getTrackbarPos(self.track_bar_name, self.window_title)
        curr_frame_descriptor = process_gray_frame(self.current_gray_frame, curr_thresh_val, display_enabled=False)
        curr_frame_descriptor.roi = self.current_roi
        curr_frame_descriptor.frame_ind_abs = self.cap.frame_cnt
        self.max_len_list.append([self.cap.frame_cnt, curr_frame_descriptor.max_len, curr_thresh_val])
        self.frame_descriptor_list.append(curr_frame_descriptor)
        # update the roi
        c = curr_frame_descriptor.largest_contour_rel
        rect = cv2.boundingRect(c)
        rect_center = [rect[0] + rect[2] // 2, rect[1] + rect[3] // 2]
        new_roi_center = [self.current_roi[0] + rect_center[0],
                          self.current_roi[1] + rect_center[1]]
        exp_ratio = 1.5
        new_roi = [new_roi_center[0] - rect[2] * exp_ratio // 2,
                   new_roi_center[1] - rect[3] * exp_ratio // 2,
                   rect[2] * exp_ratio,
                   rect[3] * exp_ratio]
        self.current_roi = new_roi
        self.current_frame = self.cap.retrieve_frame(skip_n=self.skip_n)
        cropped_frame = get_roi(self.current_frame, self.current_roi)
        self.current_gray_frame = cv2.cvtColor(cropped_frame, cv2.COLOR_BGR2GRAY)

        self.on_track_bar(curr_thresh_val)

    def on_track_bar(self, val):
        curr_frame_descriptor = process_gray_frame(self.current_gray_frame, val, display_enabled=False)
        curr_frame_descriptor.roi = self.current_roi
        curr_frame_descriptor.frame_ind_abs = self.cap.frame_cnt
        annotated_frame = annotate_frame_contour(self.current_frame, curr_frame_descriptor)
        cv2.imshow(self.window_title, annotated_frame)

    def run(self, initial_roi):
        # initialize relevant information
        self.current_roi = initial_roi

        if self.current_frame is None:
            self.current_frame = self.cap.retrieve_frame()
        cropped_frame = get_roi(self.current_frame, self.current_roi)
        self.current_gray_frame = cv2.cvtColor(cropped_frame, cv2.COLOR_BGR2GRAY)

        # create the window for display
        cv2.namedWindow(self.window_title, flags=cv2.WINDOW_GUI_NORMAL + cv2.WINDOW_AUTOSIZE)
        track_bar_callback = lambda val: self.on_track_bar(val)
        cv2.createTrackbar(self.track_bar_name, self.window_title, 127, 255, track_bar_callback)
        # display the initial image
        self.on_track_bar(127)

        is_continuous = False
        while True:
            if is_continuous:
                c = cv2.waitKey(10)
            else:
                c = cv2.waitKey(0)
            # check if exit
            if c == ord('q'):
                break
            # check if switch mode
            if is_continuous:
                if c > 0:
                    print("stop continuous mode     ")
                    is_continuous = False
            else:
                if c == ord('c'):
                    print("start continuous mode     ")
                    is_continuous = True

            # go through a frame
            try:
                for i in range(self.pause_n):
                    self.move_to_next_frame()
            except:
                print("Something wrong!!! HALT !!!")
                cv2.destroyAllWindows()
                break

            # conduct a check
            if len(self.max_len_list) > 2:
                curr_diameter = self.max_len_list[-1][1]
                last_diameter = self.max_len_list[-2][1]
                if abs(curr_diameter - last_diameter) / last_diameter > 0.1:
                    if is_continuous:
                        print("Change in diameter is too large, end continuous mode!")
                        is_continuous = False
            print("curr frame cnt = " + str(self.cap.frame_cnt), end="\r")
        cv2.destroyAllWindows()

