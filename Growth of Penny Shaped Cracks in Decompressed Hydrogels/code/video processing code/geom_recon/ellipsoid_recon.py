from geom_recon.ellipse import *
from scipy.spatial import ConvexHull
import cvxpy as cp
import numpy as np
from stl import mesh
import stl


# C_list is a list pf 3*2 matrices. Each matrix represents the x, y axis of
# the projected plane in column vector form
from geom_recon.plotting import Plotter3D


def reconstruct_ellipsoid(mat_ellipse_list, C_list, verbose=False):
    vec_ellipses = np.array([])
    rhs = None
    for i in range(len(C_list)):
        vec_ellipses = np.append(vec_ellipses, mat_ellipse_list[i].to_vec(invert=True))
        if i == 0:
            rhs = MatEllipsoid.cal_projection_rows(C_list[i])
        else:
            rhs = np.append(rhs, MatEllipsoid.cal_projection_rows(C_list[i]), axis=0)
    result = lstsq(rhs, vec_ellipses, rcond=None)
    X_vec = result[0]
    residuals = result[1]
    rank = result[2]
    if verbose:
        print("The rank of the matrix is: " + str(rank))
        print("The residual is " + str(norm(residuals)))
    if rank < 6:
        print("WARNING: the matrix does not have enough rank!")
    e3 = MatEllipsoid.vec2mat(X_vec, invert=True)
    eigenvalues = eig(e3.mat)[0]
    if verbose:
        print('The eigenvalues of the matrix is ' + str(eigenvalues))
    if min(np.array(eigenvalues)) < 0:
        print("Error, matrix not positive definite")
    return e3


# the points should come in row vectors!
# Find the inscribed ellipsoid of maximum volume.
def get_max_inscribed_ellipsoid(points):
    dim = points.shape[1]
    if not (dim == 3 or dim == 2):
        print("Dimension problem. Row vectors required!")
        return
    hull = ConvexHull(points)
    A = hull.equations[:, 0:dim]
    b = -hull.equations[:, dim]  # Negative to get convex hull in the form of A x <= b

    # set up the variables to solve the optimization problem
    B = cp.Variable((dim, dim), PSD=True)  # Ellipsoid
    d = cp.Variable(dim)  # Center

    constraints = [cp.norm(B @ A[i], 2) + A[i] @ d <= b[i] for i in range(len(A))]
    prob = cp.Problem(cp.Minimize(-cp.log_det(B)), constraints)
    optval = prob.solve()
    if optval == np.inf:
        raise Exception("No solution possible!")
    B_inv = np.linalg.inv(B.value)
    if dim == 3:
        mat_ellipsoid = MatEllipsoid(np.matmul(B_inv, B_inv), center=d.value)
        return mat_ellipsoid
    else:
        print("Not implemented")
        return None


# the points should come in row vectors!
# Find the inscribed ellipsoid of maximum volume.
def get_min_enclosing_ellipsoid(points):
    dim = points.shape[1]
    if not (dim == 3 or dim == 2):
        print("Dimension problem. Row vectors required!")
        return

    # set up the variables to solve the optimization problem
    B = cp.Variable((dim, dim), PSD=True)  # Ellipsoid
    d = cp.Variable(dim)  # Center

    constraints = [cp.norm(B @ points[i] + d, 2) <= 1 for i in range(len(points))]
    prob = cp.Problem(cp.Minimize(-cp.log_det(B)), constraints)
    optval = prob.solve()
    if optval == np.inf:
        raise Exception("No solution possible!")
    B_val = B.value
    B_inv = np.linalg.inv(B.value)
    center = -np.matmul(B_inv, d.value)
    if dim == 3:
        mat_ellipsoid = MatEllipsoid(np.matmul(B_val, B_val), center=center)
        return mat_ellipsoid
    else:
        print("Not implemented")
        return None


class MeshEllipsoidEstimate(object):
    def __init__(self, stl_file_name, frame_ind):
        # keep the frame index
        self.frame_ind = frame_ind
        # load the file
        self.file_name = stl_file_name
        self.shape_mesh = stl.mesh.Mesh.from_file(self.file_name)

        # extract points to fit ellipsoids
        x_pos = self.shape_mesh.x.flatten()
        y_pos = self.shape_mesh.y.flatten()
        z_pos = self.shape_mesh.z.flatten()
        self.pts = np.array([x_pos, y_pos, z_pos])  # this is in column vec representation

        # run the fitting for both
        self.max_inscribed = get_max_inscribed_ellipsoid(np.transpose(self.pts))
        self.min_enclosing = get_min_enclosing_ellipsoid(np.transpose(self.pts))

    def vol(self):
        # calculate mesh properties
        mesh_vol, _, _ = self.shape_mesh.get_mass_properties()
        enclosing_vol = self.min_enclosing.get_vol()
        inscribed_vol = self.max_inscribed.get_vol()
        return [mesh_vol, enclosing_vol, inscribed_vol]

    def visualize(self, s1=5, s2=1):
        my_plt = Plotter3D()
        my_plt.add_points(self.pts, s1)
        my_plt.add_points(self.max_inscribed.get_plot_pts(), s2, alpha=0.5)
        my_plt.add_points(self.min_enclosing.get_plot_pts(), s2 / 3, alpha=0.1)
        my_plt.show()

    def projection_visualization(self, s1=5, s2=1):
        my_plt = Plotter3D()
        my_plt.add_points(self.pts, s1)
        my_plt.add_points(self.max_inscribed.get_plot_pts(), s2, alpha=0.5)
        my_plt.add_points(self.min_enclosing.get_plot_pts(), s2 / 3, alpha=0.1)
        my_plt.show(0)


