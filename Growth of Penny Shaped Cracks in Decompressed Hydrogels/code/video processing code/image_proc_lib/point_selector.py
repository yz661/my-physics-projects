from image_proc_lib.cv_util import *
import circle_fit


class PointSelector(object):
    def __init__(self, img, disp_win_name="point selection", disp_size=800, color=(0, 0, 255)):
        self.img = img
        self.disp_win_name = disp_win_name
        self.disp_size = disp_size
        self.disp_ratio = self.disp_size / max(img.shape)
        self.pt_list = []
        self.color = color
        self.disp_img_original = cv2.resize(self.img,
                                            dsize=None,
                                            fx=self.disp_ratio,
                                            fy=self.disp_ratio,
                                            interpolation=cv2.INTER_NEAREST)

    def clear(self):
        self.pt_list = []

    def run(self):
        cv2.imshow(self.disp_win_name, self.disp_img_original)
        temp_func = lambda event, x, y, flags, param: self.click_callback(event, x, y, flags, param)
        cv2.setMouseCallback(self.disp_win_name, temp_func)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def click_callback(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONUP:
            self.pt_list.append([x, y])
            self.label_pts()
        if event == cv2.EVENT_RBUTTONUP:
            if len(self.pt_list) > 0:
                self.pt_list.pop()
            self.label_pts()

    def label_pts(self):
        disp_img = self.disp_img_original.copy()
        for i in range(len(self.pt_list)):
            cv2.drawMarker(disp_img, tuple(self.pt_list[i]), self.color)
        cv2.imshow(self.disp_win_name, disp_img)

    def get_pt_list(self):
        return np.array(self.pt_list) / self.disp_ratio


class BeadRegionSelector(object):
    def __init__(self, bead_region_roi=None):
        self.initialized = False
        self.bead_region = bead_region_roi
        self.bead_center_rel = None  # relative to the region of interest
        self.bead_center_abs = None  # relative to the global frame
        self.bead_rad = None
        self.disp_size = 800

    def set_disp_size(self, disp_size):
        self.disp_size = disp_size

    def initialize(self, img):  # compute the center radius based on the current frame
        # first select the roi manually
        if self.bead_region is None:
            reduction_ratio = self.disp_size/max(img.shape)
            self.bead_region = manual_select_roi(img, reduction_ratio=reduction_ratio)

        # now select points on the boundary of the bead
        img_roi = get_roi(img, self.bead_region)
        pt_selector = PointSelector(img_roi)
        pt_selector.run()
        circ_pt_list = pt_selector.get_pt_list()  # this retrieves the image coordinates

        # run circle fitting
        xc_rel, yc_rel, circ_rad, residue = circle_fit.least_squares_circle(circ_pt_list)
        if residue / circ_rad / circ_rad > 0.05:
            print("Bad circle fittig with residue: " + str(residue / circ_rad / circ_rad))
        self.bead_center_rel = np.array([xc_rel, yc_rel])
        self.bead_center_abs = np.array([xc_rel + self.bead_region[0], yc_rel + self.bead_region[1]])
        self.bead_rad = circ_rad
        self.initialized = True

    def label_frame_roi(self, img, is_make_square=True):
        if not self.initialized:
            print("BeadRegionSelector not initialized!")
            return None
        img_roi = get_roi(img, self.bead_region)
        disp_img, disp_ratio = adjust_img_size(img_roi, self.disp_size, False)
        center = self.bead_center_rel * disp_ratio
        center = center.astype(int)
        radius = int(self.bead_rad * disp_ratio)
        cv2.circle(disp_img, tuple(center), radius, [0, 0, 255], thickness=int(disp_ratio) + 1)
        if is_make_square:
            return adjust_img_size(disp_img, self.disp_size, True)[0]
        else:
            return disp_img

    def label_frame_full(self, img, is_make_square=False):
        if not self.initialized:
            print("BeadRegionSelector not initialized!")
            return None
        img_roi = img.copy()
        disp_img, disp_ratio = adjust_img_size(img_roi, self.disp_size, False)
        center = self.bead_center_abs * disp_ratio
        center = center.astype(int)
        radius = int(self.bead_rad * disp_ratio)
        cv2.circle(disp_img, tuple(center), radius, [0, 0, 255], thickness=int(2 * disp_ratio) + 1)
        if is_make_square:
            return adjust_img_size(disp_img, self.disp_size, True)[0]
        else:
            return disp_img
