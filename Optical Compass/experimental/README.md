# Experimental

## Required
- Raspberry pi with a v1 camera.
- `picamera` on Raspberry pi.
- `Jupyter Notebook` on PC with `Wolfram Engine` (see [here](https://github.com/WolframResearch/WolframLanguageForJupyter))


## Content
- `quickCapture.py` is a Python file used to capture images in a rapsberry pi. The usage should be self-evident from the comments inside the file.
- `SkyLightPolarization` is a Jupyter Notebook using a Wolfram Kernel. It is used both to analyse the experimental images and to generate theoretical predictions for comparison. The theoretical prediction is generated using [1] and the position of the sun needed for the theoretical prediction is calculated using Stellarium [2]. The usage of this Notebook is described in the comments inside the Notebook.
- `set_up_img.jpg` is the image of the experimental setup used to capture the `27_Feb_2020` data (see `example data` folder).

## References
[1] Smith, G. S. (2007). The polarization of skylight: An example from nature. American Journal of Physics, 75(1), 25-35.

[2] http://stellarium.org/ version 0.19.3

