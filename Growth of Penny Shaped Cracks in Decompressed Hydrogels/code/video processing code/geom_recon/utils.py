import cv2
from matplotlib import pyplot as plt
import numpy as np


def get_diff(img1, img2, enhance_ratio=2):
    diff_enhanced = np.abs(img1.astype(int) - img2.astype(int)) * enhance_ratio
    return diff_enhanced.astype(np.uint8)


# get a rotation matrix. Anti-clockwise angle in rad
def get_rot_mat(angle):
    return np.array([[np.cos(angle), -np.sin(angle)],
                     [np.sin(angle), np.cos(angle)]])


# takes in n*2 array where the pts are presented as rows
# use get_rot_mat to generate the rotation matrix
def rot_pts(pt_list, angle):
    col_rep = np.transpose(pt_list)
    mat = get_rot_mat(angle)
    col_rep_rotated = np.matmul(mat, col_rep)
    return np.transpose(col_rep_rotated)


# note that the angle is in degrees,
# the angle is positive when it is clockwise in the function
def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)
    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])
    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))
    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY
    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))


def print_np(mat, sf=3):
    mat = np.array(mat)
    estimate = np.mean(np.abs(mat.flatten()))
    order = np.log(estimate)/np.log(10)
    order = int(order)
    np.set_printoptions(precision=sf-1, suppress=True)
    print("1e" + str(order) + "*")
    print(mat/(10**order))
    # restore things
    np.set_printoptions(precision=8, suppress=False)
