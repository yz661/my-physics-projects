# International Physicists' Tournament (IPT) 2021 projects

I was the captain of the UK team for the International Physicists' Tournament (IPT) 2021. The competition challenged us with a list of open-ended physics questions, and we were expected to present our proposed solutions to the questions in the competition.
Together with my teammates from Cambridge, we worked on seven projects in total despite the constraints imposed by the UK national lockdown during the preparation period. We managed to present most of them during the competition, and in the end, we achieved the overall 7th place (out of the 12 particiating teams).

This folder contains the two projects I have done in preparation for the competition. 


## Nuclear Mousetraps

The "experimental simulation" of nuclear chain reactions using an array of ping-pong balls loaded on mousetraps is an impressive demonstration. However, no model has been proposed so far for the ping-pong ball chain reaction system. The target of this project is therefore to formulate a predictive model for this system under simplifying assumptions. In particular, we studied the early time growth rate of the system when periodic boundary conditions are imposed, and the exponential growth rate can be  predicted using the theory of continuous time branching process. Additionally, we also studied the case where the balls could be lost through open boundaries. The growth-vs-decay question can be answered by examining the spectrum of the "propagator" of the system, and we are able to predict the critical parameters needed to sustain the chain reaction at early times. The predictions of our theories for the two cases have been shown to be in decent agreement with our numerical experiments.

### Content
The folder contains the presentation used in the competition. The code (in Jupyter Notebook) used to generate the key experimental and theoretical plots are given as well.

## Magnetic parachute

The possibility of using a magnetic parachute to protect a load from impact when a space shuttle lands on a non-magnetic metallic surface is explored in this project. We present the theories for eddy current braking both in the low speed limit and in the general case. The simpler low speed limit theory has been validated against experiments and applied to the calculation of a magnetic parachute. It is found that the use of a magnetic parachute is highly impractical due to the $h^{-3}$ dependence of the braking force.

### Content
The folder contains both the written report submitted for the pre-selection round of the competition and the presentation used in the competition. Some information about the experimental setup is given as well.

