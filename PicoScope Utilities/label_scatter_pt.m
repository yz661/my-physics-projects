function label_scatter_pt(x, y, label, fig)
    figure(fig);
    x_range = max(x) - min(x);
    y_range = max(y) - min(y);
    dx = 0*x_range/50;
    dy = y_range/20;
    text(x+dx, y+dy, label);
end

