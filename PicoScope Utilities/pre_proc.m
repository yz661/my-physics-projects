data_dir = 'data';
data_ext = '.csv';
export_dir = 'f1_fit_export';
data_parser = @load_data_pico;
fit_list = [];

% preparations
file_list = get_files(data_dir, data_ext);
mkdir(export_dir)

% now, run through the files one by one
for i=1:length(file_list)
    full_input_file_name = strcat(data_dir, '/', file_list{i}, data_ext);
    disp(strcat('Processing ', full_input_file_name));
    [c1_t, c1_V, c2_t, c2_V] = data_parser(full_input_file_name);
    fig = figure('visible',false);
    c1_fit = f1_fit(c1_t, c1_V);
    c2_fit = f1_fit(c2_t, c2_V);
    plt_f1_fit(c1_t, c1_V, c1_fit,...
               c2_t, c2_V, c2_fit, fig)
    export_img_file_name = strcat(export_dir, '/', file_list{i}, '.png');
    exportgraphics(fig, export_img_file_name,'Resolution',300)
    close(fig);
    fit_list = [fit_list; [c1_fit, c2_fit]];
end

save(strcat(export_dir, '\pre_proc.mat'))