function [c1_t, c1_V, c2_t, c2_V] = load_data_hantek(file_name)
    max_pt = 10000000;
    fid = fopen(file_name);

    line = fgetl(fid);
    while ~strcmp(line,'#CHANNEL:CH1')
        line = fgetl(fid);
    end
    % get out the clock rate
    line = fgetl(fid);
    sample_rate = hantek_sample_rate(line);
    line = fgetl(fid);
    n_pt = sscanf(line, '#SIZE=%d') - 10;
    line = fgetl(fid);
    if ~strcmp(line, '#UNITS:V')
        disp(line);
        disp("Error, unit not matched");
    end
    line = fgetl(fid);
    line = fgetl(fid);
    c1_t = zeros(n_pt, 1);
    c1_V = zeros(n_pt, 1);

    for i=1:min(n_pt, max_pt)
        line = fgetl(fid);
        c1_t(i) = i/sample_rate;
        c1_V(i) = str2double(line);
    end


    while ~strcmp(line,'#CHANNEL:CH2')
        line = fgetl(fid);
    end
    % get out the clock rate
    line = fgetl(fid);
    sample_rate = hantek_sample_rate(line);
    line = fgetl(fid);
    n_pt = sscanf(line, '#SIZE=%d')  - 10;
    line = fgetl(fid);
    if ~strcmp(line, '#UNITS:V')
        disp(line);
        disp("Error, unit not matched");
    end
    line = fgetl(fid);
    line = fgetl(fid);
    c2_t = zeros(n_pt, 1);
    c2_V = zeros(n_pt, 1);

    for i=1:min(n_pt, max_pt)
        line = fgetl(fid);
        c2_t(i) = i/sample_rate;
        c2_V(i) = str2double(line);
    end
    fclose(fid);
end
