import cv2
from matplotlib import pyplot as plt
import numpy as np
import time, sys
from IPython.display import clear_output
from scipy import stats
import csv
import os
import pathlib
import pickle
import subprocess
from stl import mesh
import stl

# load custom modules
from image_proc_lib.file_util import *
from image_proc_lib.cv_util import *
from image_proc_lib.point_selector import *
from image_proc_lib.image_proc import *
from geom_recon.utils import *


class ViewManager(object):
    # first frame is the frame with the background only
    # start frame has the bead and the bead holder
    def __init__(self, first_frame, start_frame, angle, label, reduction_ratio=0.5):
        self.angle = angle
        self.label = label
        self.view_roi = manual_select_roi(first_frame, reduction_ratio=reduction_ratio)
        self.first_frame = first_frame
        self.start_frame = start_frame
        # this angle is used to rotate the img coordinates positive if clockwise
        self.img_rot_angle = None
        # this angle is used to rotate the corrected coordinates. positive if anti-clockwise
        self.pt_corrected_rot_angle = None
        # bead center in the image coordinate
        self.bead_center_abs = None
        self.bead_rad = None
        # some additional variables
        self.diff_img = get_diff(self.first_frame, self.start_frame)

    def get_rotation_angle(self):
        diff_img_cropped = get_roi(self.diff_img, self.view_roi)
        print("select points on a vertical line")
        ps = PointSelector(diff_img_cropped)
        ps.run()
        vertical_pt_list = ps.get_pt_list()
        pt_x = vertical_pt_list[:, 0]
        pt_y = vertical_pt_list[:, 1]
        # this thing comes from the image, but the image has kind of a flipped coordinate system!
        slope, intercept, r_value, p_value, std_err = stats.linregress(pt_x, pt_y)
        self.img_rot_angle = np.pi / 2 - np.arctan(slope)
        self.pt_corrected_rot_angle = self.img_rot_angle

    def get_bead_center(self, is_disp=False):
        bs = BeadRegionSelector(self.view_roi)
        bs.initialize(self.diff_img)
        if is_disp:
            disp_cv_img(bs.label_frame_roi(self.start_frame))
        self.bead_center_abs = bs.bead_center_abs
        self.bead_rad = bs.bead_rad
        print(bs.bead_rad)
        return [self.bead_center_abs, bs.label_frame_roi(self.start_frame)]

    def rotate_pi(self):
        if self.img_rot_angle is None:
            print("Angle not initialized yet")
            return
        else:
            self.img_rot_angle = self.img_rot_angle + np.pi
            self.pt_corrected_rot_angle = self.pt_corrected_rot_angle + np.pi

    def rotate_img(self, img, full=False):
        if full:
            return rotate_bound(img, self.img_rot_angle * 180 / np.pi)
        else:
            img_cropped = get_roi(img, self.view_roi)
            return rotate_bound(img_cropped, self.img_rot_angle * 180 / np.pi)

    # if img coord is returned, nothing is done after the rotation
    # if corrected coord is used, y is flipped to make the axis right handed
    # after the transformation, the coordinates are rotated upright
    # and the origin is the center of the bead
    def transform_contour(self, rel_contour, contour_roi, ret_img_coord=False):
        pts_list = rel_contour
        # remove the extra bracket if needed
        if len(rel_contour.shape) == 3:
            pts_list = rel_contour[:, 0]
        # transform to global coordinates
        pts_list = pts_list + [contour_roi[0], contour_roi[1]]
        # transform to coordinates relative to bead center
        pts_list = pts_list - self.bead_center_abs
        # rotate in img coordinate
        # this is very confusing, because the y-axis is flipped,
        # if we rotate clockwise by theta, the matrix will be
        # [[cos(theta), -sin(theta)], [sin(theta), cos(theta)]]
        # this is actuallt the matrix for counter-clockweise rotation in normal coordinates...
        rotated_pts_list = rot_pts(pts_list, self.img_rot_angle)
        # flip the y-coordinates if needed
        # print(rotated_pts_list)
        if not ret_img_coord:
            rotated_pts_list[:, 1] = -rotated_pts_list[:, 1]
        return rotated_pts_list
