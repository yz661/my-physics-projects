import pathlib
import os
import csv

def get_from_upper_dir(filename):
    path = pathlib.Path(os.getcwd())
    return str(path.parent) + "\\" + filename


def exist_file(rel_file_name):
    path = os.getcwd()
    file_path = path + "\\" + rel_file_name
    return os.path.isfile(file_path)


def exist_directory(rel_dir):
    path = os.getcwd()
    dir_path = path + "\\" + rel_dir
    return os.path.isdir(dir_path)


def full_path(name):
    return os.getcwd() +"\\" + name


def export_to_csv(rel_file_name, data, header=None, allow_overwrite=False):
    # check if the file already exists
    if exist_file(rel_file_name) and not allow_overwrite:
        print("File " + rel_file_name + " already exists. Overwrite is not allowed!")
        print("Export failed")
        return False
    # export data
    with open(rel_file_name, 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        if not (header is None):
            csv_writer.writerow(header)
        for i in range(len(data)):
            csv_writer.writerow(data[i])
    return True


def open_file_w_with_check(rel_file_name, allow_overwrite=False, mode='w'):
    if exist_file(rel_file_name) and not allow_overwrite:
        print("File " + rel_file_name + " already exists. Overwrite is not allowed!")
        print("Export failed")
        return None
    return open(full_path(rel_file_name), mode)


