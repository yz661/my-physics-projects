function plt_f1_fit(t1, x1, f1, t2, x2, f2, fig)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here
    figure(fig);
    t_st = min(t1);
    dt = t1(2) - t1(1);
    t_plot = 2*pi*3/f1.w;
    t_end = min(t_st + t_plot, min(max(t1), max(t2)));
    n_pt = round((t_end - t_st)/dt) - 1;
    x1_pred = f1.c + f1.A*cos(f1.w*t1(1:n_pt) + f1.phi);
    x2_pred = f2.c + f2.A*cos(f2.w*t2(1:n_pt) + f2.phi);
    hold on
    yyaxis left
    ylabel('Channel 1 (V)');
    scatter(t1(1:n_pt), x1(1:n_pt), 1);
    plot(t1(1:n_pt), x1_pred);
    yyaxis right
    ylabel('Channel 2 (V)');
    scatter(t2(1:n_pt), x2(1:n_pt), 1);
    plot(t2(1:n_pt), x2_pred);
    xlabel('t/s')
    hold off
end

