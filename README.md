# My Physics Projects

# Overview
This repository contains a list of physics-related projects I have done in the past. 
Each project is documented in its own folder. A presentation (if available) for the project concerned can be found in its folder.  The data analysis/computational code and some example data for the project might be provided as well.

# Content

## Cambridge Part II long labs
### Coherence and Information in a Fibre Interferometer

Interferometry provides useful methods for power spectrum measurement
and velocity detection. In this experiment, we focus on a specific
fibre-based Mach-Zehnder (MZ) interferometer. The interferometer is
first used to extract information about the power spectrum of a super
luminescent diode (SLD) and a Fabry-Perot (FP) laser using
contrast measurement. The full-width-half-maximum (FWHM) of the SLD
 is estimated to be (38 ± 4) nm and the free spectral range (FRS)
of the FP laser is estimated to be *Δλ* = (1.14±0.03) nm. The effect
of introducing optical amplifiers into the arms of the interferometer is
then demonstrated to be no more than introducing a constant fraction of
incoherent intensities in the interfering beams. Finally, an oscillating
loudspeaker is characterised using interferometric velocity measurement.
The loudspeaker is shown to be well modelled by a simple harmonic
oscillator at low frequencies (< 20 Hz) and its resonant frequency is
estimated to be (69 ± 7) Hz.

While this is a lab with a prescribed manual, we have managed to develop a few new measurement techniques that greatly improved our data-taking efficiency. Additionally, with the help of a custom-written Python interface for PicoScope, we were also able to acquire some interesting data that would be otherwise impossible to obtain using the standard PicoScope software.

## Laser system for cold atom (K39) experiments

This is the project I did in Prof Zoran Hadzibabic's group over the summer of 2021. The group focuses on the experimental study of Bose-Einstein condensates (BEC). A new setup for K39 BEC was under construction when I joined the group for the summer. In the new setup, the temperature of the K39 atoms will be first reduced using laser cooling (Doppler cooling and Gray Molasses), and BEC can be achieved after evaporative cooling. Once BEC is achieved, further experimental studies can happen. The target of my experimental project is to construct the laser system to supply the laser frequencies needed for the laser cooling process (2D MOT, the 3D MOT, and the Gray Molasses cooling).

The setup has been partially constructed by the end of the project, and the dominant problems limiting the construction of the setup have been identified. With two months of lab work, the project has helped me build up the essential experimental skills to efficiently work in a laser optics lab.


## International Physicists' Tournament (IPT) 2021 projects

I was the captain of the UK team for the International Physicists' Tournament (IPT) 2021. The competition challenged us with a list of open-ended physics questions, and we were expected to present our proposed solutions to the questions in the competition.
Together with my teammates from Cambridge, we worked on seven projects in total despite the constraints imposed by the UK national lockdown during the preparation period. We managed to present most of them during the competition, and in the end, we achieved the overall 7th place (out of the 12 particiating teams).

A description of the two projects I have done is given here.

### Nuclear Mousetraps

The "experimental simulation" of nuclear chain reactions using an array of ping-pong balls loaded on mousetraps is an impressive demonstration. However, no model has been proposed so far for the ping-pong ball chain reaction system. The target of this project is therefore to formulate a predictive model for this system under simplifying assumptions. In particular, we studied the early time growth rate of the system when periodic boundary conditions are imposed, and the exponential growth rate can be  predicted using the theory of continuous time branching process. Additionally, we also studied the case where the balls could be lost through open boundaries. The growth-vs-decay question can be answered by examining the spectrum of the "propagator" of the system, and we are able to predict the critical parameters needed to sustain the chain reaction at early times. The predictions of our theories for the two cases have been shown to be in decent agreement with our numerical experiments.

### Magnetic parachute

The possibility of using a magnetic parachute to protect a load from impact when a space shuttle lands on a non-magnetic metallic surface is explored in this project. We present the theories for eddy current braking both in the low speed limit and in the general case. The simpler low speed limit theory has been validated against experiments and applied to the calculation of a magnetic parachute. It is found that the use of a magnetic parachute is highly impractical due to the $h^{-3}$ dependence of the braking force.


## Giving Hydrogels the "Bends"

This project was done in the summer of 2020 under the supervision of Dr Lefauve and Dr Etzold from DAMTP.The project aimmed to investigate the potential growth of fracturing cracks in tissue as a result of decompression sickness.

We report that the decompression of soft brittle materials can lead to the growth of internal gas-filled cracks. These cracks are oblate spheroids ('penny shape'), whose main radius grows linearly in time, irreversibly fracturing the surrounding material. Our optical measurements in hydrogels characterise andquantify the three-dimensional crack geometry and growth rate. These results are in good agreementwith our analytical model coupling fracture mechanics and gas diffusion, and predicting the dependenceon the mechanical properties, gas diffusivity and super-saturation conditions (gas pressure, solubility, temperature).  Our results suggest a new potential mechanism for decompression sickness in scubadiving and for indirect optical measurements of the fracture properties of hydrogels.

![quick result summary](https://gitlab.com/yz661/my-physics-projects/-/blob/master/Growth%20of%20Penny%20Shaped%20Cracks%20in%20Decompressed%20Hydrogels/quick_image.PNG)



## Optical Compass

This project was motivated by a question in the International Physicist's Tournament (IPT) 2020, where we were tasked to build a compass using polarization effect. 

Sky light from different angles is polarized to various degree depending on the position of the sun. The degree of linear polarization can be modelled by considering the Rayleigh scattering of direct sun light in the atmosphere. In this project, we aimmed to verify the proposed theory[1] of the degree of linear polarization in the sky light using an optical setup centered around a calibrated Raspberry pi v1 camera. The setup is able to make quantitative measurements of the degree of linear polarization in the sky light, and we have achieved qualitative agreement between the model and experiments. The application of the theory to the development of an optical compass using sky light polarization can be explored.

![Sample Result](https://gitlab.com/yz661/my-physics-projects/-/blob/master/Optical%20Compass/example%20data/sample_image.PNG)

## PicoScope Utilities

This is a very short project motivated by the pain of manual data taking during my 'remote' IB practicals.

Bode plots are extensively used in the investigation of electrical circuit systems. However, taking a Bode plot manually with a signal generator and a PicoScope can be very painful due to the tediousness of phase measurement in the PicoScope software. To accelerate and partially automate the acquisition of Bode plots, a set of MATLAB code has been developed in this project. The code takes in data exported from the PicoScope and performs automated data analysis to generate Bode plots with error estimates. The code is able to work reliably over a wide range of settings and offer significant improvement to the speed at which Bode plots can be acquired.

Apart from the code used to create the Bode plots, a few other utilities are also given.

## SYPT and IYPT projects

These are some of the projects I have done during my involvement with the Singapore Young Physicist's Tournament (SYPT) and the International Young Physicist Tournament (IYPT).

## Cusps in a Cylinder
This project was done by me during my preparation for SYPT 2018.

When a horizonatal cylinder is partially filled with a viscous fluid and rotated around its axis, a thin film of liquid will be pulled out from the liquid resevoir at the bottom due to the rotation of the wall. When the liquid film re-enters the bottom resevoir, a liquid front will be formed. Depending on the speed of the rotation, various instabilities could develop around the liquid front. In this project, the instabilites in this system (cusp formation and sloshing instability) were investigated both experimentally and theoretically. 2D steady state solutions to the problem was calculated using the finite difference method following [1] and validated against experiments. The growth of instabilities along the axial directly were analysed using linear stability analysis, and some qualitative insgihts were obtained through the analysis.

![Quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/cusp_illus.PNG)

## Soy Sauce Optics
This project was done in collaboration with Max Tan whom I mentored for IYPT 2019.

When a laser beam passes through a thin layer of a highly absorbing liquid (e.g. 200 um of soy sauce), thermal lens effect could be observed as the heated liquid undegoes a change in refractive index. This phenomenon was investigated in this project both experimentally and theoretically. Experimentally, we have observed both the weak thermal lens effect (where the beam spot simply expands) and the strong thermal lens effect (where significant interference pattern is visible). A numerical transient theory was developed in the Fraunhofer regime and shown to be in very good agreement with our experimental results.

![Quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/soy_sauce_illus.PNG)

## Acoustic Levitation
This short project was motivated by a quation in IYPT 2018.

Acoustic standing waves are known to be able to create a trapping Gorkov potential, and this small objects to be levitated acoustically. However, recent developments suggests that a trapping Gorkov potential can be achieved without a standing wave. Following [2], I built a single sided phased array of ultrasounic transducers that is able to levitate small objects. The phased array was controlled by an Arduino mega (which was in turn controlled by a Python code on my PC). The setup was demonstrated to be working, and it was able to move the levitated small particle in 3D. A video of the setup in action can be found [here](https://drive.google.com/file/d/1Tpm9-7tknDXtD_6Y3QHdEolfOpLb6fCu/view?usp=sharing).

![Acoustic levitation in action](https://gitlab.com/yz661/my-physics-projects/-/blob/master/SYPT%20and%20IYPT%20projects/levitator_illus.PNG)


[1] Hosoi, A. E., & Mahadevan, L. (1999). Axial instability of a free-surface front in a partially filled horizontal rotating cylinder. Physics of Fluids, 11(1), 97-106.

[2] Marzo, A., Seah, S. A., Drinkwater, B. W., Sahoo, D. R., Long, B., & Subramanian, S. (2015). Holographic acoustic elements for manipulation of levitated objects. Nature communications, 6, 8661.


## The application of cross-linked carbon nanotube and molybdenum-disulphide in energy storage.
This project was done in 2017 under the supervision of Dr Derrick Fam at Institute of Materials Research and Engineering (IMRE) in Singapore.

Cross-linked carbon nanotube (CNT) and molybdenum-disulphide (MoS2) composite was synthesized and 
characterized in this study. The electrochemical performance of the material in 1-Ethyl-3-methylimidazolium
bis(trifluoromethylsulfonyl)imide (EMI TFSI), 1 M sulfuric acid, and 0.5 M sodium sulphate was
electrochemically characterized in both three-electrode cell and two-electrode cell settings. Cyclic voltammetry
(CV), electrochemical impedance spectroscopy (EIS), and galvanostatic charge-discharge (GCD) were performed.
The introduction of MoS2 into the composite gives superior performance, particularly in organic and neutral
electrolyte. Specific capacitance of 19 F/g and 21F/g in CNT/MoS2 composite electrodes has been recorded in
EMI TFSI and in 0.5 M sodium sulphate respectively. Significant factors limiting the performance of the material,
namely, the presence of iron impurities and the large interfacial impedance between the free-standing electrode
and current collector, were identified. The study also indicates future direction on the optimization of device
performance, including the suppression of iron redox reaction, the tuning of MoS2 content and electrolyte
concentration, and the reduction of interfacial impedance.

![quick illustration](https://gitlab.com/yz661/my-physics-projects/-/blob/master/The%20application%20of%20cross-linked%20CNT%20and%20MoS2%20in%20energy%20storage/quick_illus.PNG)

## Miscellaneous

