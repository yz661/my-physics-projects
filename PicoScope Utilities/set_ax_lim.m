function set_ax_lim(x, y, fig)
    figure(fig);
    x_range = max(x) - min(x);
    y_range = max(y) - min(y);
    xlim([min(x) - 0.2*x_range, max(x) + 0.2*x_range])
    ylim([min(y) - 0.2*y_range, max(y) + 0.2*y_range])
end

