function [c1_t, c1_V, c2_t, c2_V] = load_data_pico(file_name)
    name_len = length(file_name);
    file_name_no_ext = file_name(1:name_len-4);
 
    mat_export_file_name = strcat(file_name_no_ext, '_cache.mat');
    if isfile(mat_export_file_name)
        load(mat_export_file_name, 'temp');
        disp(strcat(file_name, ' using cache'));
        c1_t = temp.c1_t;
        c1_V = temp.c1_V;
        c2_t = temp.c2_t;
        c2_V = temp.c2_V;
        return;
    end
    
    T = readtable(file_name);
    TT = T{:,:};

    fid = fopen(file_name);    
    line = fgetl(fid);
    line = fgetl(fid);
    fclose(fid);
    units = split(line, ',');
    switch units{1}
        case '(us)'
            time_factor = 1e-6;
        case '(ms)'
            time_factor = 1e-3;
        case '(s)'
            time_factor = 1;
        case '(ns)'
            time_factor = 1e-9;
        otherwise
            disp(line);
            disp('Error, not recognized');
    end
    
    switch units{2}
        case '(uV)'
            V_factor1 = 1e-6;
        case '(mV)'
            V_factor1 = 1e-3;
        case '(V)'
            V_factor1 = 1;
        case '(nV)'
            V_factor1 = 1e-9;
        otherwise
            disp(line);
            disp('Error, not recognized');
    end
    
    switch units{3}
        case '(uV)'
            V_factor2 = 1e-6;
        case '(mV)'
            V_factor2 = 1e-3;
        case '(V)'
            V_factor2 = 1;
        case '(nV)'
            V_factor2 = 1e-9;
        otherwise
            disp(line);
            disp('Error, not recognized');
    end
    
    c1_t = time_factor*TT(:, 1);
    c2_t = time_factor*TT(:, 1);
    c1_V = V_factor1*TT(:, 2);
    c2_V = V_factor2*TT(:, 3);
end