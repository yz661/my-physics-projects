# Cambridge Part II Physics long labs

This folder contains the long lab projects I have done as part of the Cambridge third year physics course.

## Coherence and Information in a Fibre Interferometer

Interferometry provides useful methods for power spectrum measurement
and velocity detection. In this experiment, we focus on a specific
fibre-based Mach-Zehnder (MZ) interferometer. The interferometer is
first used to extract information about the power spectrum of a super
luminescent diode (SLD) and a Fabry-Perot (FP) laser using
contrast measurement. The full-width-half-maximum (FWHM) of the SLD
 is estimated to be (38 ± 4) nm and the free spectral range (FRS)
of the FP laser is estimated to be *Δλ* = (1.14±0.03) nm. The effect
of introducing optical amplifiers into the arms of the interferometer is
then demonstrated to be no more than introducing a constant fraction of
incoherent intensities in the interfering beams. Finally, an oscillating
loudspeaker is characterised using interferometric velocity measurement.
The loudspeaker is shown to be well modelled by a simple harmonic
oscillator at low frequencies (< 20 Hz) and its resonant frequency is
estimated to be (69 ± 7) Hz.

While this is a lab with a prescribed manual, we have managed to develop a few new measurement techniques that greatly improved our data-taking efficiency. Additionally, with the help of a custom-written Python interface for PicoScope, we were also able to acquire some interesting data that would be otherwise impossible to obtain using the standard PicoScope software.

### Content
The report and the final presentation are given. Key data processing code (including the PicoScope interface) is given in the appendix of the report.

