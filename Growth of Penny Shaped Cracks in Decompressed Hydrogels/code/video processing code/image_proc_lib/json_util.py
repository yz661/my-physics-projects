import json
from json import JSONEncoder
import numpy


class NumpyArrayEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, numpy.integer):
            return int(obj)
        elif isinstance(obj, numpy.floating):
            return float(obj)
        if isinstance(obj, numpy.ndarray):
            return obj.astype(int).tolist()
        return JSONEncoder.default(self, obj)


def json_dumps(obj):
    return json.dumps(obj.__dict__, cls=NumpyArrayEncoder)

