import cv2
from matplotlib import pyplot as plt
import numpy as np
import time, sys
from IPython.display import clear_output
from scipy import stats
import csv
import os
import pathlib
import pickle
import subprocess
from stl import mesh
import stl

# load custom modules
from image_proc_lib.file_util import *
from image_proc_lib.cv_util import *
from image_proc_lib.point_selector import *
from image_proc_lib.image_proc import *
from geom_recon.view_manager import *


class ProcessedDescriptor(object):
    def __init__(self, raw_descriptor):
        self.raw_descriptor = raw_descriptor
        self.bead_rad = None
        # this should contain corrected coordinates
        self.processed_cnt = None
        self.angle = None
        self.label = None


def process_view_init(first_frame, start_frame, descriptor, angle, label, rot_pi=False, reduction_ratio=0.5):
    vm = ViewManager(first_frame, start_frame, angle, label, reduction_ratio=reduction_ratio)
    vm.get_rotation_angle()
    if rot_pi:
        vm.rotate_pi()
    ret = vm.get_bead_center(False)
    disp_cv_img(vm.rotate_img(ret[1], full=True))

    descriptor_hull = descriptor.get_convex_hull_rel()[:, 0]
    # print(descriptor_hull)
    descriptor_roi = descriptor.get_roi()
    cnt = vm.transform_contour(descriptor_hull, descriptor_roi, ret_img_coord=False)
    processed_descriptor = ProcessedDescriptor(descriptor)
    processed_descriptor.bead_rad = vm.bead_rad
    processed_descriptor.processed_cnt = cnt
    processed_descriptor.angle = angle
    processed_descriptor.label = label
    return [processed_descriptor, vm]


def process_view(descriptor, vm):
    descriptor_hull = descriptor.get_convex_hull_rel()[:, 0]
    # print(descriptor_hull)
    descriptor_roi = descriptor.get_roi()
    cnt = vm.transform_contour(descriptor_hull, descriptor_roi, ret_img_coord=False)
    processed_descriptor = ProcessedDescriptor(descriptor)
    processed_descriptor.bead_rad = vm.bead_rad
    processed_descriptor.processed_cnt = cnt
    processed_descriptor.angle = vm.angle
    processed_descriptor.label = vm.label
    return processed_descriptor

def process_view_all_pts(descriptor, vm):
    all_pts = []
    cnt_list = descriptor.largest_contour_rel
    for i in range(len(cnt_list)):
        all_pts = all_pts + cnt_list[i][:, 0].tolist()
    all_pts = np.array(all_pts)
    descriptor_roi = descriptor.get_roi()
    cnt = vm.transform_contour(all_pts, descriptor_roi, ret_img_coord=False)
    processed_descriptor = ProcessedDescriptor(descriptor)
    processed_descriptor.bead_rad = vm.bead_rad
    processed_descriptor.processed_cnt = cnt
    processed_descriptor.angle = vm.angle
    processed_descriptor.label = vm.label
    return processed_descriptor