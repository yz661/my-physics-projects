from time import sleep
from picamera import PiCamera
import time

camera = PiCamera(resolution=(1280, 720), framerate=30)
# Set ISO to the desired value
camera.iso = 100
# Wait for the automatic gain control to settle
print("Start recoding wait to stabilise the camera")
sleep(2)
# Now fix the values
camera.shutter_speed = 3000
camera.exposure_mode = 'off'
g = camera.awb_gains
camera.awb_mode = 'off'
camera.awb_gains = (1.28, 1.57)  # g
print("Camera status: exposure, awb, ISO")
print(camera.exposure_speed)
print(camera.awb_gains)
print(camera.ISO)

while True:
    file_name = raw_input("new capture:")
    print("Start capturing to " + file_name + ".jpg")
    camera.capture(file_name + ".jpg")
    print("Finish capturing to " + file_name + ".jpg")

    f = open(file_name + ".txt", "w")
    f.write("Camera status: exposure, awb, ISO, capture time \r\n")
    f.write(str(camera.exposure_speed) + "\r\n")
    f.write(str(camera.awb_gains) + "\r\n")
    f.write(str(camera.ISO) + "\r\n")
    curr_time = str(time.localtime(time.time()))
    f.write(curr_time + "\r\n")
    f.close()
    print("Finish writing data to " + file_name + ".txt")
    print("ready for next capture")
